package edu.columbia.opinion.classifications;

public interface Classification {

	public static final int POLARITY = 0;
	public static final int SUBJECTIVITY = 1;
	
	String getName();
	String[] getValues();
	String getValue(int position);
	int getType();
}
