package edu.columbia.opinion.classifications;

public class OpinionClassification implements Classification {

	private static final String[] VALUES = {"subjective", "objective"};
	public static final String NAME = "subjectivity_so";
	private int _type = Classification.SUBJECTIVITY;
		
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String[] getValues() {
		return VALUES;
	}
	
	public String getValue(int position) {
		return VALUES[position];
	}

	public int getType() {
		return _type;
	}
}
