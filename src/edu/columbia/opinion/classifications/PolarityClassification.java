package edu.columbia.opinion.classifications;

public class PolarityClassification implements Classification {

	private static String[] VALUES;
	private static String NAME;
	private int _type = Classification.POLARITY;
	public static String NEUTRAL_NAME = "polarity_pno";
	public static String POLARITY_NAME = "polarity_pn";
	public PolarityClassification(boolean neutral) {
		
		VALUES = new String[neutral ? 3 : 2];
		VALUES[0] = "positive";
		VALUES[1] = "negative";
		
		if (neutral) {
			VALUES[2] = "neutral";
			NAME = NEUTRAL_NAME;
		}
		else NAME = POLARITY_NAME;	
	}
	
	@Override
	public String getName() {
		return NAME;
	}
	@Override
	public String[] getValues() {
		return VALUES;
	}
	
	public String getValue(int position) {
		return VALUES[position];
	}

	public int getType() {
		return _type;
	}

}
