package edu.columbia.opinion.io;

import java.util.*;

public class ResultSentence {

	List<Phrase> _phrases;
	
	public ResultSentence() {
		_phrases = new ArrayList<Phrase>();
	}
	
	public void add(Phrase phrase) {
		_phrases.add(phrase);
	}
	
	public String getSentence() {
		String sentence = "";
		for (Phrase phrase : _phrases) {
			sentence += phrase.getPhrase() + " ";
		}
		return sentence.trim();
	}
	
	public String getSentenceSubjectivity() {
		String sentence = "";
		for (Phrase phrase : _phrases) {
			sentence += phrase.sentiment() + " ";
		}
		return sentence.trim();
	}
	
	public Phrase getPhrase(int index) {
		return _phrases.get(index);
	}
	
	public List<Phrase> getPhrases() {
		return _phrases;
	}
	
	public String toString() {
		String string = "";
		
		for (Phrase phrase : _phrases) {
			string += "[" + phrase.getPhrase() + "]/" + phrase.sentiment() + " ";
		}
		
		return string;
	}
}
