package edu.columbia.opinion.io;

import java.util.ArrayList;
import java.util.List;

import edu.columbia.opinion.process.*;

public class UnseenDataReader extends AbstractDataReader {

	 public static void main(String[] args) {
		 UnseenDataReader reader = new UnseenDataReader();
		 reader.getChunks("\"/RBR/B-NP And/NNP/I-NP yes/UH/B-INTJ ,/,/O it/PRP/B-NP will/MD/B-VP be/VB/I-VP posted/VBN/I-VP when/WRB/B-ADVP I/PRP/B-NP am/VBP/B-VP through/RP/B-PRT >/EX/B-UCP :/:/O \"/POS/B-NP");
	 }
	
	protected SentenceBuilder _builder;
	
	protected String _file;
	protected boolean _chunked;
	
	public UnseenDataReader() {};
	
	public UnseenDataReader(String file, int classifier, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, String useNoun, boolean chunked) {
		_chunked = chunked;
		_builder = new SentenceBuilder(classifier, useWordNet, useWiktionary, useEmoticons, useNoun);
		_file = file;
	}
	
	public UnseenDataReader(List<String> sentences, int classifier, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, String useNoun, boolean chunked) {
		_builder = new SentenceBuilder(classifier, useWordNet, useWiktionary, useEmoticons, useNoun);
		makeSentences(sentences,chunked);
	}
	
	public UnseenDataReader(List<String> sentences, int classifier, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, String useNoun) {
		this(sentences,classifier, useWordNet, useWiktionary, useEmoticons, useNoun, false);
	}

	public UnseenDataReader(List<String> sentences, int classifier, boolean chunked) {
		this(sentences,classifier,false,false,false,"NNP",chunked);
	}
	
	protected List<Sentence> makeSentences() {
        List<String> lines = Utils.readLines(_file);
        return makeSentences(lines,_chunked);
	}
	
	protected List<Sentence> makeSentences(List<String> lines, List<String> phrases, List<String> chunkLines) {
		List<Sentence> sentences = new ArrayList<Sentence>();

        for (int index = 0; index < lines.size(); index++) {
        	String [] indices = phrases.get(index).split(",");
            int [] updateIndices = updateIndices(lines.get(index), chunkLines.get(index),Integer.parseInt(indices[0]),Integer.parseInt(indices[1]));
			Sentence sentence = _builder.buildSentence(chunkLines.get(index), Integer.valueOf(updateIndices[0]), Integer.valueOf(updateIndices[1]), null);
            if (edu.columbia.opinion.module.OpinionRunner.DEBUG) System.out.println(sentence.toString());
    		sentences.add(sentence);
        }
        return sentences;
	}
	
	protected List<Sentence> makeSentences(List<String> lines, boolean chunked) {
		List<Sentence> sentences = new ArrayList<Sentence>();

        for (String line : lines) {
        	if (!chunked) {
	            Sentence sentence = _builder.buildSentence(line, 0, line.split(" ").length, null);
	            sentences.add(sentence);
        	}
        	else {
        		List<String> chunks = getChunks(line);
        		for (String chunk : chunks) {
        			
        			String [] indices = chunk.split("-");
        			Sentence sentence = _builder.buildSentence(line, Integer.valueOf(indices[0]), Integer.valueOf(indices[1]), null);
        			sentences.add(sentence);
        		}
        	}
        }
        return sentences;
	}
	
	protected List<String> getChunks(String chunkStr) {
		
		List<String> chunked = new ArrayList<String>();
		
        // split chunk string at spaces
        String[] splitChunkStr = chunkStr.split("\\s+");
        
        int startIndex = 0, endIndex = 0;
        
        for (String tok : splitChunkStr) {
        	String chunkTag = "";
        		
        	try {
	            chunkTag = tok.substring(tok.lastIndexOf("/")+1);
        	} catch (Exception e) {
        		System.err.println("[SentenceBuilderImpl.annotate] Error splitting " + tok);
        		continue;
        	}
        	
            // get main chunk label (B,O,I)
            String chunkLabel = "";
            if (chunkTag.indexOf("-") >= 0) {
                chunkLabel = chunkTag.substring(0,1);
            }
            
            // B starts new chunk
            if (chunkLabel.equals("B") && endIndex > 0) {
                chunked.add(startIndex + "-" + (endIndex-1));
                startIndex = endIndex;
            }
            
            endIndex++;
        }
        // add the last chunk
        chunked.add(startIndex + "-" + (endIndex-1));
        return chunked;
	}
}
