package edu.columbia.opinion.io;

public class Phrase {

	String _phrase;
	String _sentiment;
	double _confidence;
	
	public Phrase (String phrase, String sentiment, double confidence) {
		_phrase = phrase;
		_sentiment = sentiment;
		_confidence = confidence;
	}
	
	public String getPhrase() {
		return _phrase;
	}
	
	public String sentiment() {
		return _sentiment;
	}
	
	public double getConfidence() {
		return _confidence;
	}
}
