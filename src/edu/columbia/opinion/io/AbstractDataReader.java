package edu.columbia.opinion.io;

import java.util.*;
import java.io.*;

import processing.Contractions;
import processing.EmoticonDictionary;
import processing.GeneralUtils;

import weka.core.Instances;
import wiktionary.Wiktionary;
import edu.columbia.opinion.process.*;
import net.didion.jwnl.JWNL;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;
import edu.stanford.nlp.process.Morphology;

public abstract class AbstractDataReader {

	protected List<Sentence> _sentences;
	protected boolean _useWordNet;
	protected boolean _useWiktionary;
	protected boolean _useEmoticons;
	protected String _useNoun;
	protected Contractions _contractions;
	
	public AbstractDataReader() {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String dir = prop.getProperty("contractions_directory");

			_contractions = new Contractions(dir + "/contractions-parserversion.txt");
		} catch (Exception e) {
			System.err.println("[AbstractDataReader] error loading contractions: " + e);
			e.printStackTrace();
		}
	}
	
	public AbstractDataReader(List<Sentence> sentences) {
		super();
		_sentences = sentences;
	}
	
	public List<Sentence>getSentences( ) {
		if (_sentences == null) {
			_sentences = makeSentences();
		}
		return _sentences;
	}
	
	public Instances getInstances() {
		return null;
	}

	protected abstract List<Sentence> makeSentences();
	
	public void computeStatistics(Collection<Sentence> sentences) {
		
		HashSet<String> words = new HashSet<String>();
		HashSet<String> wordsAll = new HashSet<String>();
		double sentenceLength = 0;
		double wordCount = 0;
		
		try {
			
			EmoticonDictionary emoticonDictionary = SentenceBuilder.loadDictionary();
			Wiktionary wiktionary = SentenceBuilder.loadWiktionary();
			DAL dal = DAL.getInstance();
			JWNL.initialize(new FileInputStream("config/file_properties.xml"));
	        Dictionary wordNet = Dictionary.getInstance();
			
			double inDAL = 0;
			double inWordNet = 0;
			double inWiktionary = 0;
			double inEmoticon = 0;
			double isPunctuation = 0;
			double isNNP = 0;
			double notCovered = 0;
			double isNumber = 0;
			double isWordLengthening = 0;
			
			for (Sentence sentence : sentences) {
				for (PolarWord word : sentence.getPolarWordArray(true)) {
					
					String w = word.getWord().toLowerCase();
					
					if (words.contains(w)) continue;
					
					words.add(w);
					
					if (w.matches("\\p{Punct}+") && !emoticonDictionary.hasEmoticon(w)) {
						isPunctuation++;
						continue;
					}
					if (w.matches("[0-9]+")) {
						isNumber++;
						continue;
					}
					
					POS pos = SentenceBuilder.getJwnlPos(word.getTag());
					
					// check each dictionary here
					if (dal.hasWord(w)) inDAL++;
					else if (emoticonDictionary.hasEmoticon(w)) inEmoticon++;
					else if (word.getTag().matches("NNP|NNPS")) isNNP++;
					else if (w.matches(".*(\\w)(\\1)+.*") && (dal.hasWord(w.replaceAll("(\\w)(\\1)+", "$1")) || w.matches(".*(\\w)(\\1)(\\1)+.*"))) isWordLengthening++;
					else if (pos != null && wordNet.getIndexWord(pos,w) != null) inWordNet++;
					else if (pos != null && wordNet.getIndexWord(pos, Morphology.lemmaStatic(w,pos.toString(),true)) != null) inWordNet++;
					//else if (pos != null && w.matches("[A-Za-z]*") && wordNet.lookupIndexWord(pos, w) != null) inWordNet++;
					//else if (wiktionary != null && !wiktionary.query(w, false).isEmpty()) inWiktionary++;
					else if (wiktionary != null && !wiktionary.query(w, true).isEmpty()) inWiktionary++;
					else { 
						notCovered++;
						//System.out.println("\"" + sentence.getPlainString(true).replaceAll("\\\"", "\"") + "\", " + w);
					}
				}
				for (String word : sentence.getPlainStringArray(false)) {
					wordsAll.add(word.toLowerCase());
				}
				wordCount += sentence.getPlainStringArray(false).length;
				sentenceLength += sentence.getPlainString(false).length();
			}
			
			double total = inDAL + inEmoticon + inWordNet + inWiktionary + isPunctuation + isNumber + notCovered + isNNP + isWordLengthening;
			java.text.DecimalFormat df = new java.text.DecimalFormat("#.#");
			System.out.println("There are " + wordsAll.size() + " words in the vocabulary.");
			System.out.println("There are " + words.size() + " words in the target phrase vocabulary.");
			System.out.println("The average sentence contains " + wordCount/sentences.size() + " words.");
			System.out.println("The average sentence contains " + sentenceLength/sentences.size() + " characters.");
			System.out.println("The DAL covers " + inDAL + " words for a coverage of " + df.format((inDAL/total)*100) + "%.");
			System.out.println("The Emoticon Dictionary covers " + inEmoticon + " words for a coverage of " + df.format((inEmoticon/total)*100) + "%.");
			System.out.println("NNP (Post DAL) covers " + isNNP + " words for a coverage of " + df.format((isNNP/total)*100) + "%.");
			System.out.println("Word Lengthening covers " + isWordLengthening + " words for a coverage of " + df.format((isWordLengthening/total)*100) + "%.");
			System.out.println("The WordNet Dictionary covers " + inWordNet + " words for a coverage of " + df.format((inWordNet/total)*100) + "%.");
			System.out.println("The Wiktionary Dictionary covers " + inWiktionary + " words for a coverage of " + df.format((inWiktionary/total)*100) + "%.");
			System.out.println("Punctuation and Numbers covers " + (isPunctuation + isNumber) + " words for a coverage of " + df.format(((isPunctuation+isNumber)/total)*100) + "%.");
			System.out.println(notCovered + "/" + total + " words are not covered by dictionaries (" + df.format((notCovered/total)*100) + "%).");
		} catch (Exception e) {
			System.err.println("[AbstractDataReader.computeStatistics] Error computing statistics: " + e);
			e.printStackTrace();
		}
		//System.exit(0);
	}
	
	/**
	 * To Test:
	 * 
	 * Dates/NNP/B-NP of/IN/B-PP future/JJ/B-NP events/NNS/I-NP ,/,/O etc/FW/B-NP ././O ././O
	 * mmm/NN/B-NP I/PRP/B-NP am/VBP/B-VP home/NN/I-VP tomorrow/NN/B-NP sometime/RB/B-PP after/IN/I-PP 6/7/CD/I-PP ,/,/O wan/VB/B-VP na/RP/B-PRT come/VB/B-VP round/NN/B-NP then/RB/B-ADVP ?/./O
	 */
	/*public int [] updateIndices(String originalSentence, String chunkSentence, int start, int end) {
		
		int [] indices = {start,end};
		
		String [] chunkWords = null;
		ArrayList<String> originalWords = null;
		
		try {
			
			if (originalSentence.indexOf("I will be staring at it like") >= 0) {
				System.out.print("");
			}
			*/
			//chunkWords = chunkSentence.replaceAll("/[a-zA-Z!\"#$%&\'(),-.:;?{}`]*/([BIO]-[a-zA-Z!\"#$%&\'(),-.:;?{}`]*)( |$)", " ").split("\\s+");
			//chunkWords = chunkSentence.replaceAll("/[\\p{Graph}&&[^/]]*/([BIO]-?[\\p{Graph}&&[^/]]*)( |$)", " ").replaceAll("''", "\"").replaceAll("``","\"").split("\\s+");
			/*
			originalWords = new ArrayList<String>(Arrays.asList(_contractions.swapContractions(originalSentence).split(" ")));
					
			if (originalSentence.indexOf("/") >= 0 && chunkSentence.indexOf("|") >= 0) {
				for (int index = 0; index < chunkWords.length; index++) {
					chunkWords[index] = chunkWords[index].replaceAll("\\|", "/");
				}
			}
			
			// indices are the same
			if (chunkWords.length == originalWords.size()) return indices;
			if (originalWords.size()+1 == end) {
				indices[1] = chunkWords.length+1;
				return indices;
			}
			
			int originalIndex = 0;
			boolean sDone = false; boolean eDone = false;
			String word = "";
			
			for (int index = 0; index < chunkWords.length; index++) {
				word += chunkWords[index];
				int startIndex = index;
				
				// if its a contraction convert it so that it will be the same.
				//if (!word.equals(originalWords.get(originalIndex)) && word.replaceAll("\\s+", "").equals(_contractions.swapContractions(originalWords.get(originalIndex)).replaceAll("\\s+", ""))) {
				//	word = originalWords.get(originalIndex);
				//}
				
				// The word got messed up from the chunker -- concatenate chunked words
				while (!word.equals(originalWords.get(originalIndex)) && word.length() < originalWords.get(originalIndex).length()) {
					index++;
					
					word+= chunkWords[index];
					
					if (!word.equals(originalWords.get(originalIndex)) && word.replaceAll("\\s+", "").equals(_contractions.swapContractions(originalWords.get(originalIndex)).replaceAll("\\s+", ""))) {
						word = originalWords.get(originalIndex);
					}
					//in = true;
				}
				
				// The original sentence has extra things - usually a space. Remove it.
				while (word.length() > originalWords.get(originalIndex).length()) {
					//if (!in) index--;
					originalWords.set(originalIndex,originalWords.get(originalIndex) + originalWords.get(originalIndex+1));
					originalWords.remove(originalIndex+1);
					if (originalIndex < end) end--;
					if (originalIndex < start) start--;
				}
				
				if (!originalWords.get(originalIndex).equals(word)) {
					continue;
				}
				
				if (start == originalIndex) {
					indices[0] = startIndex;
					sDone = true;
				}
				if (end == originalIndex) {
					indices[1] = index;
					eDone = true;
				}
				originalIndex++;
				

				if (sDone && eDone) {
					return indices;
				}
				word = "";
			}
		} catch (Exception e) {
			System.err.println("[AbstractDataReader.updateIndices] error updating indices: " + e);
			System.err.println(originalSentence);
			e.printStackTrace();
		}
		
		indices[1] = chunkWords.length + 1;
		return indices;
	}*/
	
	/**
	 * To Test:
	 * 
	 * Dates/NNP/B-NP of/IN/B-PP future/JJ/B-NP events/NNS/I-NP ,/,/O etc/FW/B-NP ././O ././O
	 * mmm/NN/B-NP I/PRP/B-NP am/VBP/B-VP home/NN/I-VP tomorrow/NN/B-NP sometime/RB/B-PP after/IN/I-PP 6/7/CD/I-PP ,/,/O wan/VB/B-VP na/RP/B-PRT come/VB/B-VP round/NN/B-NP then/RB/B-ADVP ?/./O
	 */
	public int [] updateIndices(String originalSentence, String chunkSentence, int start, int end) {
		
		int [] indices = {start,end};
		
		String [] chunkWords = chunkSentence.replaceAll("/[\\p{Graph}&&[^/]]*/([BIO]-?[\\p{Graph}&&[^/]]*)( |$)", " ").replaceAll("\\\\\\|", "/").replaceAll("\"", "''").replaceAll("`","'").split("\\s+");
		String [] originalWords = originalSentence.replaceAll("\"", "''").replaceAll("`","'").split(" ");
		
		try {
			
			if (originalSentence.indexOf("Musical awareness") >= 0 
					|| originalSentence.indexOf("looks like im going to the UGA") >= 0) {
				System.out.print("");
			}
			
			if (chunkWords.length == originalWords.length) {
				return indices;
			}
			// extra words are at the end 0 - 20 21 words
			// chunk > 22 0 - 21
			// chunk < 20 0 - 19
			if (start == 0 && originalWords.length == end+1) {
				indices[1] = chunkWords.length-1;
				indices[0] = 0;
				return indices;
			}
			int oIndex = 0;
			
			for (int cIndex = 0; cIndex < chunkWords.length-1 && oIndex < originalWords.length-1; cIndex++) {
				
				// indices updated
				if (oIndex > start && oIndex > end) {
					//System.err.print(Arrays.toString(Arrays.copyOfRange(originalWords, start,end+1)) + " <----> ");
					//System.err.println(Arrays.toString(Arrays.copyOfRange(chunkWords, indices[0],indices[1]+1)));
					return indices;
				}
				// the extra words are at the end
				/*if (oIndex+1 >= originalWords.length) {
					//System.err.print(Arrays.toString(Arrays.copyOfRange(originalWords, start,end+1)) + " <----> ");
					//System.err.println(Arrays.toString(Arrays.copyOfRange(chunkWords, indices[0],indices[1]+1)));
					indices[1] = chunkWords.length-1;
					return indices;
				}*/
				
				// extra spaces in original sentence (chunker ignores extra spaces)
				while (originalWords[oIndex].equals("")) {
					if (cIndex < indices[0]) { indices[0]--; indices[1]--; }
					else if (cIndex <= indices[1] ) { indices[1]--; }
					oIndex++;
				}
				
				if (originalWords[oIndex].toLowerCase().equals(chunkWords[cIndex].toLowerCase())) {
					oIndex++;
					continue;
				}
				else if (oIndex+1 >= originalWords.length || cIndex+1 >= chunkWords.length) continue;
				// chunk and original don't match but no extra word. eg " vs ''
				else if (originalWords[oIndex+1].toLowerCase().equals(chunkWords[cIndex+1].toLowerCase())) {
					oIndex++;
					continue;
				}
				
				int distance = findIndex(originalWords[oIndex+1],chunkWords,cIndex+1);
				
				// extra word(s) in chunk: eg don't do not
				if (distance >= 0 && originalWords[oIndex+1].toLowerCase().indexOf(chunkWords[cIndex+distance+1].toLowerCase()) >= 0) {
					if (cIndex < indices[0]) { indices[0]+=distance; indices[1]+=distance; }
					else if (cIndex <= indices[1] ) { indices[1]+=distance; }
					cIndex+=distance;
					oIndex++;
					continue;
				}
				
				distance = findIndex(chunkWords[cIndex+1],originalWords,oIndex+1);
				
				if (distance >= 0 && chunkWords[cIndex+1].toLowerCase().indexOf(originalWords[oIndex+distance+1].toLowerCase()) >= 0) {
					if (cIndex < indices[0]) { indices[0]-=distance; indices[1]-=distance; }
					else if (cIndex <= indices[1] ) { indices[1]-=distance; }
					oIndex+=distance+1;
					continue;
				}
				System.err.println("Mismatch!! " + originalSentence);
				System.err.println("Mismatch!! " + chunkSentence.replaceAll("/[\\p{Graph}&&[^/]]*/([BIO]-?[\\p{Graph}&&[^/]]*)( |$)", " "));
				System.err.println("-----");
				return null;
				// extra words at end of sentence
				// indices[1] = chunkWords.length-1;
				// return indices;
			}
		} catch (Exception e) {
			System.err.println("[AbstractDataReader.updateIndices] error updating indices: " + e);
			System.err.println(originalSentence);
			e.printStackTrace();
		}
		
		//System.err.print(Arrays.toString(Arrays.copyOfRange(originalWords, start,end+1)) + " <----> ");
		//System.err.println(Arrays.toString(Arrays.copyOfRange(chunkWords, indices[0],indices[1]+1)));
		//indices[1] = chunkWords.length-1;
		return indices;
	}
	
	private int findIndex(String word, String [] words, int start) {
		word = word.toLowerCase();
		
		for (int index = start; index < words.length; index++) {
			if (word.indexOf(words[index].toLowerCase()) >= 0) return index-start;
		}
		return -1;
	}
	
	protected void setSentences(List<Sentence> sentences) {
		_sentences = sentences;
	}
}
