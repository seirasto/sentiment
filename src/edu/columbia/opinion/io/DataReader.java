package edu.columbia.opinion.io;

import java.util.Collection;
import weka.core.Instances;
import edu.columbia.opinion.process.Sentence;

public interface DataReader {

	Collection<Sentence> getSentences();
	Instances getInstances();
}
