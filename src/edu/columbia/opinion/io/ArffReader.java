/**
 * 
 */
package edu.columbia.opinion.io;

import java.util.List;
import java.io.*;
import edu.columbia.opinion.process.Sentence;
import weka.core.Instances;

/**
 * @author sara
 *
 */
public class ArffReader extends AbstractDataReader {

	Instances _instances;
	/**
	 * 
	 */
	public ArffReader(String file) {
		try {
			BufferedReader reader = new BufferedReader(
	        new FileReader(file));
			_instances = new Instances(reader);
			reader.close();
		} catch (Exception e) {
			System.err.println("[ArffReader] error loading instances: " + e );
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see sentiment.readers.AbstractDataReader#makeSentences()
	 */
	@Override
	protected List<Sentence> makeSentences() {
		return null;
	}
	
	public Instances getInstances() {
		return _instances;
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
