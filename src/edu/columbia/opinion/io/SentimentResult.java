package edu.columbia.opinion.io;

import java.util.*;
import java.io.*;
import java.util.regex.*;

import processing.GeneralUtils;

import edu.columbia.opinion.process.Utils;

public class SentimentResult {

	String _text; // string associated with result
	String _classLabel;
	double _classValue; //0 or 1
	double _trueValue; // annotated value
	double _confidence;
	int _start; // start and end position of phrase
	int _end;
	int _startWord;
	int _endWord;
	
	public static void main(String[] args) {
		
		/*String directory = "/proj/nlp/users/sara/sentiment/corpus/stocktwit/";
		
		SentimentResult.writeSubjectivityAndPolarity(directory + "correct.txt.subj", directory + "correct.txt.op", directory + "correct.txt",.7);
		SentimentResult.writeSubjectivityAndPolarity(directory + "incorrect.txt.subj", directory + "incorrect.txt.op", directory + "incorrect.txt",.7);
		*/
		//String file = "/proj/nlp/users/sara/agreement/res/Chris+Spencer/bal-agreement.tar.txt.emo.con.pos.chk.emo";
		//SentimentResult.writeSubjectivityAndPolarity(file + ".sbj", file + ".pol", file, .7);
		
		String dir = "/proj/nlp/users/sara/agreement/create_debate//processed//processed-debate.show.";
		
		String f1 = "Do_games_permote_violence_or_self_defence_lj.xml/debate.show.Do_games_permote_violence_or_self_defence_lj.xml.txt.pol";
		//String f2 = "Does_coffee_help_you_or_hurt_you_lj.xml/debate.show.Does_coffee_help_you_or_hurt_you_lj.xml.txt.pol";
		
		List<String> lines = GeneralUtils.readLines(dir + f1);
		
		for (String line : lines) {
			SentimentResult.ProcessLabeledSentence(line);
		}
	}
	
	
	/**
	 * New Result
	 * 
	 * @param text String associated with the result
	 * @param classLabel subjective/objective or positive/negative/neutral
	 * @param classValue 0 or 1
	 * @param confidence value between 0 and 1
	 */
	public SentimentResult(String text, String classLabel, double classValue, double confidence) {
		this(text,classLabel,classValue,confidence,-1,0,text.length(),0,text.split("\\s+").length);
	}

	/**
	 * New Result
	 * 
	 * @param text String associated with the result
	 * @param classLabel subjective/objective or positive/negative/neutral
	 * @param classValue 0 or 1
	 * @param confidence value between 0 and 1
	 * @param trueValue annotated value (0 or 1)
	 */
	public SentimentResult(String text, String classLabel, double classValue, double confidence, double trueValue) {
		this(text,classLabel,classValue,confidence,trueValue,0,text.length(),0,text.split("\\s+").length);
	}
	
	/**
	 * New Result
	 * 
	 * @param text String associated with the result
	 * @param classLabel subjective/objective or positive/negative/neutral
	 * @param classValue 0 or 1
	 * @param confidence value between 0 and 1
	 * @param trueValue annotated value (0 or 1)
	 * @param startC start position of phrase in terms of characters
	 * @param endC end position of phrase in terms of characters
	 * @param start start position of phrase in terms of words
	 * @param end end position of phrase in terms of words
	 */
	public SentimentResult(String text, String classLabel, double classValue, double confidence, double trueValue, int startC, int endC, int start, int end) {
		_text = text;
		_classLabel = classLabel;
		_classValue = classValue;
		_trueValue = trueValue;
		_confidence = confidence;
		_start = startC;
		_end = endC;
		_startWord = start;
		_endWord = end;
	}
	
	public String getText() {
		return _text;
	}
	
	public int numWords() {
		return _text.split("\\s+").length;
	}
	
	public double getClassValue() {
		return _classValue;
	}
	
	public double getTrueValue() {
		return _trueValue;
	}
	
	public String getClassLabel() {
		return _classLabel;
	}	
	
	public double getConfidence() {
		return _confidence;
	}
	
	public int getStart() {
		return _start;
	}
	
	public int getEnd() {
		return _end;
	}
	
	public int getStartWord() {
		return _startWord;
	}
	
	public int getEndWord() {
		return _endWord;
	}
	
	public void setConfidence(double confidence) {
		// flip class
		if (confidence < .5 && (_classLabel.equals("subjective") || _classLabel.equals("objective"))) {
			confidence = 1 - confidence;
			_classLabel =  _classLabel.equals("subjective") ? "objective" : "subjective";
			//if (_classLabel.equals("positive") || _classLabel.equals("negative")) _classLabel = "neutral";
			_classValue =  _classValue == 0 ? 1: 0;
		}
			
		_confidence = confidence;
	}
	
	public String toStringPhrase(double threshold) {
		
		String label = "";
		
		if (_classLabel.equals("subjective")) label = "s";
		else if (_classLabel.equals("objective")) label = "o";
		else if (_classLabel.equals("positive")) label = "+";
		else if (_classLabel.equals("negative")) label = "-";
		else if (_classLabel.equals("neutral")) label = "o";
		
		if (_confidence < threshold) label = "";
		
		return "[" + _text.substring(_start,_end).trim() + "]/" + label;		
	}
	
	public String toString() {
		
		String label = "";
		
		if (_classLabel.equals("subjective")) label = "s";
		else if (_classLabel.equals("objective")) label = "o";
		else if (_classLabel.equals("positive")) label = "+";
		else if (_classLabel.equals("negative")) label = "-";
		else if (_classLabel.equals("neutral")) label = "o";
		
		return _text.substring(0,_start) + "[" + _text.substring(_start,_end)
			+ "]/" + label + _text.substring(_end);
	}
	
	public static void writePhrasesToFile(String outputFile, List<SentimentResult> results, boolean subjectivity, double threshold) {
		
		System.err.println("[SentimentResult.writeToFile] Writing to " + outputFile + (subjectivity ? ".sbj" : ".pol"));
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile  + (subjectivity ? ".sbj" : ".pol")));
			int index = 0;
			
			for (SentimentResult result : results) {
				if (index > 0 && result.getStart() == 0) writer.write("\n");
 				writer.write(result.toStringPhrase(threshold) + " ");
				index++;
			}
			writer.write("\n");
			writer.close();
			
		} catch (Exception e) {
			System.err.println("[SentimentResult.writeToFile] " + outputFile);
			e.printStackTrace();
		}
	}
	
	public static void writeToFile(String outputFile, List<SentimentResult> results, boolean subjectivity) {
		
		System.err.println("[SentimentResult.writeToFile] Writing to " + outputFile + (subjectivity ? ".sbj" : "pol"));
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile  + (subjectivity ? ".sbj" : "pol")));
			
			for (SentimentResult result : results) {
				writer.write(result.toString() + "\n");
			}
			
			writer.close();
			
		} catch (Exception e) {
			System.err.println("[SentimentResult.writeToFile] " + outputFile);
			e.printStackTrace();
		}
	}
	
	public static List<ResultSentence> getSentences(List<SentimentResult> results) {
		
		List<ResultSentence> sentences = new ArrayList<ResultSentence>();
		
		ResultSentence sentence = new ResultSentence();
		
		int index = 0;
		
		for (SentimentResult result : results) {
			if (index > 0 && result.getStart() == 0) {
				sentences.add(sentence);
				sentence = new ResultSentence();
			}
			String phrase = result.getText().substring(result.getStart(), result.getEnd()).trim();
			
			String subjectivity = "";
			
			if (result.getClassLabel().equals("subjective")) subjectivity = "s";
			else if (result.getClassLabel().equals("objective")) subjectivity = "o";
			else if (result.getClassLabel().equals("positive")) subjectivity = "+";
			else if (result.getClassLabel().equals("negative")) subjectivity = "-";
			else if (result.getClassLabel().equals("neutral")) subjectivity = "o";
			
			//String subjectivity = result.getClassLabel().equals("subjective") ? "s" : "o";		
			Phrase p = new Phrase(phrase,subjectivity,result.getConfidence());
			sentence.add(p);
			index++;
		}
		// add last sentence
		sentences.add(sentence);
		
		return sentences;
	}
	
	public static ResultSentence ProcessLabeledSentence(String sentence) {
		Pattern pattern = Pattern.compile("\\[.+?\\]/[os+-]?");
		Matcher m = pattern.matcher(sentence);
		
		ResultSentence s = new ResultSentence();
		
		while (m.find()) {
			String phrase = m.group();
			String subjectivity = phrase.substring(phrase.length()-1);
			if (subjectivity.equals("/")) subjectivity = "";
			phrase = phrase.substring(1,phrase.lastIndexOf("/")-1);
			Phrase p = new Phrase(phrase, subjectivity,1);
			s.add(p);
		}
		return s;
	}
	
	public static ResultSentence processAnnotatedSentence(String sentence, String annotations) {
		
		String [] ann = annotations.split("\\|");
		
		ResultSentence s = new ResultSentence();
		
		for (String annotation : ann) {
			try {
				String [] info = annotation.split(",");
				int start = wordToChar(sentence, Integer.valueOf(info[0]));
				int end = wordToChar(sentence, Integer.valueOf(info[1])+1) - 1;
				String phrase = sentence.substring(start,end);
				Phrase p = new Phrase(phrase, info[2],1);
				s.add(p);
			} catch (Exception e) {
				System.err.println(sentence + ", " + annotations + " on: " + annotation);
				e.printStackTrace();
			}
		}
		
		return s;
		
	}
	
	private static int wordToChar(String sentence, int wPosition) {
		int cPosition = 0;
		
		String [] words = sentence.split(" ");
		
		for (int index = 0; index < wPosition; index++) {
			cPosition += words[index].length() + 1;
		}
		return cPosition;		
	}
	
	public static ResultSentence mergeSentenceSubjectivityAndPolarity(String subjectivity, String polarity, double threshold) {
		ResultSentence s = ProcessLabeledSentence(subjectivity);
		ResultSentence p = ProcessLabeledSentence(polarity);
		
		ResultSentence merged = new ResultSentence();
		
		for (int index = 0; index < p.getPhrases().size(); index++) {
			if (!s.getPhrase(index).sentiment().equals("s") || s.getPhrase(index).getConfidence() < threshold) {
				Phrase phrase = new Phrase(s.getPhrase(index).getPhrase(),"",s.getPhrase(index).getConfidence());
				merged.add(phrase);
			}
			else merged.add(p.getPhrase(index));
		}
		
		return merged;
	}
	
	public static void writeSubjectivityAndPolarity(String sFile, String pFile, String outputFile, double threshold) {
		List<String> sLines = Utils.readLines(sFile);
		List<String> pLines = Utils.readLines(pFile);
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile  + ".op"));
			
			for (int index = 0; index < sLines.size(); index++) {
				ResultSentence sentence = mergeSentenceSubjectivityAndPolarity(sLines.get(index),pLines.get(index), threshold);
				writer.write(sentence.toString() + "\n");
			}
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
