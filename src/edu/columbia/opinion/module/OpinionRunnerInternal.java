package edu.columbia.opinion.module;

import java.util.*;


import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.process.PolarityDataProcessor;
import edu.columbia.opinion.process.SubjectivityDataProcessor;
import edu.columbia.opinion.process.TestDataProcessor;
import edu.columbia.opinion.train.io.AnnotatedDataReader;
import edu.columbia.opinion.train.io.AnnotatedPolarityDataReader;

/**
 * @author sara
 *
 */
public class OpinionRunnerInternal extends OpinionRunner {

	public static void main(String[] args) {
		
		String testFile = args[0];
		
		OpinionRunnerInternal runner = new OpinionRunnerInternal(true, true, true, true, 1000, false);
		ArrayList<SentimentResult> subjectivity = runner.runSubjectivity(testFile,"/proj/nlp/users/sara/sentiment/models/subjectivity/model/train-livejournal+mpqa_full+wikipedia-wo-wi-em-sm-nnp-bal-n1000-subjectivity_so");
		ArrayList<SentimentResult> polarity = runner.runPolarity(testFile, false, "/proj/nlp/users/sara/sentiment/models/polarity/model/train-livejournal+mpqa_full+wikipedia-wo-wi-em-sm-nnp-n1000-bal-polarity_pn");
		ArrayList<SentimentResult> merged = runner.combineSubjectivityAndPolarity(subjectivity, polarity);
		
		for (SentimentResult result : merged) {
			System.out.println(result);
		}
	}
	
	public OpinionRunnerInternal(boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean tuneToAnnotations) {
		super("model/", wordnet, wiktionary, emoticons, socialMedia, nGrams, tuneToAnnotations);
	}
	
	public OpinionRunnerInternal(String modelDirectory, boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean tuneToAnnotations) {
		super(modelDirectory, wordnet, wiktionary, emoticons, socialMedia, nGrams,false, tuneToAnnotations);
	}
	
	/**
	 * 
	 * Read in settings. The settings are used to load the existing training file
	 */
	public OpinionRunnerInternal(String modelDirectory, boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean reprocess, boolean tuneToAnnotations) {
		super(modelDirectory, wordnet, wiktionary, emoticons, socialMedia, nGrams, reprocess, tuneToAnnotations);
	}
	
	public AbstractDataReader getReader(List<String> files, String directory, Classification classification) {
		
		List<String> processed = new ArrayList<String>();
		List<String> text = new ArrayList<String>();
		List<String> annotation = new ArrayList<String>();
		
		for (int index = 0; index < files.size(); index++) {
			text.add(directory + "/" + files.get(index) + "_text.txt");
			annotation.add(directory + "/" + files.get(index) + "_annotation.txt");
			processed.add(OpinionRunner.preprocess(directory + "/" + files.get(index) + "_text.txt",false));
		}		
		return getReader(processed, text, annotation, classification); 
	}
	
	public AbstractDataReader getReader(List<String> processedSentences, List<String> originalSentences, List<String> annotation, Classification classification) {	
		if (classification.getType() == Classification.POLARITY) {
			return new AnnotatedPolarityDataReader(true, classification, processedSentences, originalSentences,annotation,_useWordNet,_useWiktionary,_useEmoticons,false,_useNoun, classification.getName().equals("polarity_pno") ? true : false);
		}
		else {
			return new AnnotatedDataReader(true, classification, processedSentences, originalSentences,annotation,_useWordNet,_useWiktionary,_useEmoticons,false,_useNoun);
		}
	}
	
	public ArrayList<SentimentResult> runSubjectivity(String testFile, String model) {
		OpinionClassification classification =  new OpinionClassification();
		AbstractDataReader reader = getReader(Arrays.asList(testFile + ".emo.pos.chk.emo"), 
				Arrays.asList(testFile), Arrays.asList(testFile + ".phrases"), classification);
		TestDataProcessor processor = new SubjectivityDataProcessor(classification,_socialMedia, true,_nGrams, false);
		ArrayList<SentimentResult> results = test(model, processor, reader);
    	return results;
	}
	
	public ArrayList<SentimentResult> runPolarity(String fileName, boolean neutral, String model) {
		PolarityClassification classification =  new PolarityClassification(neutral);
		AbstractDataReader reader = getReader(Arrays.asList(fileName + ".emo.pos.chk.emo"), 
				Arrays.asList(fileName), Arrays.asList(fileName + ".phrases"), classification);
		TestDataProcessor processor = new PolarityDataProcessor(classification,_socialMedia, true, _nGrams, false);
		ArrayList<SentimentResult> results = test(model, processor, reader);
    	return results;
	}
}
