package edu.columbia.opinion.module;

import crf.chunker.*;
import crf.tagger.*;
import java.io.*;
import java.util.*;

import processing.NlpUtils;
import processing.SwapEmoticons;
//import processing.Contractions;
import weka.core.Instance;
import weka.core.Instances;
import weka.classifiers.Classifier;
import weka.core.converters.ArffLoader;
//import weka.core.converters.ArffSaver;

import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.process.*;

//import edu.stanford.nlp.ling.CoreLabel;
//import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;



/**
 * @author sara
 *
 */
public class OpinionRunner {

	public static final boolean DEBUG = false;
	
	/**
	 * These options cannot be changed unless a new model is generated.
	 */
	TestDataProcessor _processor;
	//Logistic _classifier;
	Classification _classification;
	String _classifier = "-logistic";
	public boolean _useEmoticons = true;
	public boolean _useWiktionary = true;
	public boolean _useWordNet = true;
	public boolean _socialMedia = true;
	public boolean _neutral = true;
	public boolean _tuneToAnnotations = false;
	public String _trainingFile;
	public int _type;
	public String _useNoun = "NNP";
	public int _nGrams;
	String _modelDirectory = "model/";
	public boolean _reprocess;
	
	private static LexicalizedParser _lexicalizedParser;
	private static TokenizerFactory<CoreLabel> _tokenizerFactory;
	
	public static crf.chunker.Maps _chunkerMaps;
	public static crf.chunker.Dictionary _chunkerDict;
	public static ChunkingData _chunkerData;
	public static crf.chunker.Model _chunkerModel;
	
	public static crf.tagger.Maps _taggerMaps;
	public static crf.tagger.Dictionary _taggerDict;
	public static TaggingData _taggerData;
	public static crf.tagger.Model _taggerModel;
	
	public static void main(String[] args) {
		
		String testFile = args[0];
		
		OpinionRunner runner = new OpinionRunner(true, true, true, true, 100, false);
		ArrayList<SentimentResult> subjectivity = runner.runSubjectivity(testFile,false);
		ArrayList<SentimentResult> polarity = runner.runPolarity(testFile,false);
		ArrayList<SentimentResult> merged = runner.combineSubjectivityAndPolarity(subjectivity, polarity);
		
		for (SentimentResult result : merged) {
			System.out.println(result);
		}
	}
	
	public OpinionRunner(boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean tuneToAnnotations) {
		this("model/", wordnet, wiktionary, emoticons, socialMedia, nGrams, tuneToAnnotations);
	}
	
	public OpinionRunner(String modelDirectory, boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean tuneToAnnotations) {
		this(modelDirectory, wordnet, wiktionary, emoticons, socialMedia, nGrams,false, tuneToAnnotations);
	}
	
	/**
	 * 
	 * Read in settings. The settings are used to load the existing training file
	 */
	public OpinionRunner(String modelDirectory, boolean wordnet, boolean wiktionary, boolean emoticons, boolean socialMedia, int nGrams, boolean reprocess, boolean tuneToAnnotations) {
		_useWiktionary = wiktionary;
		_useEmoticons = emoticons;
		_useWordNet = wordnet;
		_socialMedia = socialMedia;
		_nGrams = nGrams;
		_reprocess = reprocess;
		_tuneToAnnotations = tuneToAnnotations;
		
		String fileName = "";
		fileName += "-n" + _nGrams;
		if (_useWordNet) fileName += "-wo";
		if (_useWiktionary) fileName += "-wi";
		if (_useEmoticons) fileName += "-em";
		if (_socialMedia) fileName += "-sm";
		//fileName += ".model";
		
		_trainingFile = fileName;
		_modelDirectory = modelDirectory;
		
		_lexicalizedParser = LexicalizedParser.loadModel();
		_tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "invertible=true");

	}
	
	public ArrayList<SentimentResult> combineSubjectivityAndPolarity(ArrayList<SentimentResult> sbjResults, ArrayList<SentimentResult> polResults) {
		ArrayList<SentimentResult> results = new ArrayList<SentimentResult>();
		
		for (int i = 0; i< polResults.size(); i++) {
			if (sbjResults == null) results.add(polResults.get(i));
			else if (sbjResults.get(i).getClassLabel().equals("subjective")) {
				results.add(polResults.get(i));
			}
			else {
				results.add(sbjResults.get(i));
			}
		}
		return results;	
	}
	
	public ArrayList<SentimentResult> runSubjectivity(String testFile) {
		return runSubjectivity(testFile, true);
	}
	
	public ArrayList<SentimentResult> runSubjectivity(String testFile, boolean chunked) {
		
		// load model file from configuration
		TestDataProcessor processor = new SubjectivityDataProcessor(new OpinionClassification(),_socialMedia,true,_nGrams,_tuneToAnnotations);
	
    	AbstractDataReader reader = getReader(OpinionRunner.preprocess(testFile,_reprocess), Classification.SUBJECTIVITY, chunked);
    	ArrayList<SentimentResult> results = test(_modelDirectory + "subjectivity" + _trainingFile, processor, reader);
    	return results;
	}
	
	public String getTrainingModel(boolean subjectivity) {
		return _modelDirectory + (subjectivity ? "subjectivity" : "polarity") + _trainingFile;
	}
	
	public ArrayList<SentimentResult> runPolarity(String testFile) {
		return runPolarity(testFile, true);
	}
	
	public ArrayList<SentimentResult> runPolarity(String testFile, boolean chunked) {
		
		// load model file from configuration
		TestDataProcessor processor = new PolarityDataProcessor(new PolarityClassification(_neutral),_socialMedia,true,_nGrams,_tuneToAnnotations);
	
    	AbstractDataReader reader = getReader(OpinionRunner.preprocess(testFile,_reprocess), Classification.POLARITY, chunked);
    	ArrayList<SentimentResult> results = test(_modelDirectory + "polarity" + _trainingFile, processor, reader);
    	return results;
	}
	
	public AbstractDataReader getReader(List<String> sentences) {
		return new UnseenDataReader(sentences,_type,_useWordNet,_useWiktionary,_useEmoticons,_useNoun);
	}
	
	public AbstractDataReader getReader(String file, int type, boolean chunked) {
		return new UnseenDataReader(file,type,_useWordNet,_useWiktionary,_useEmoticons,_useNoun,chunked);
	}
	
	public ArrayList<SentimentResult> test(String model, TestDataProcessor processor,  AbstractDataReader reader) {
		
		try {
			// load model 
			ObjectInputStream ois = new ObjectInputStream(
	                 new FileInputStream(model + ".model"));
			Classifier classifier = (Classifier) ois.readObject();
			ois.close();
			
			// load arff of attributes
			ArffLoader loader = new ArffLoader();
			loader.setFile(new File(model + ".arff"));
			Instances attributes = loader.getStructure();
			attributes.setClassIndex(attributes.numAttributes() - 1);
			
			Instances test = processor.getTestSet(reader,attributes);
			
			/*ArffSaver saver = new ArffSaver();
			//train.delete();
			saver.setInstances(test);
		    saver.setFile(new File(model + "-test.arff"));
		    saver.writeBatch();*/
		    
			return classify(classifier,test,processor.getTestSentences(), processor.getClassification());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<SentimentResult> classify(Classifier classifier, Instances test, List<Sentence> sentences, Classification classification) {
		ArrayList<SentimentResult> results = new ArrayList<SentimentResult>();
    	
		for (int i=0; i< test.numInstances(); i++) {
			
			try {
				Instance instance = test.instance(i);
				double result = classifier.classifyInstance(instance);
				double confidence = classifier.distributionForInstance(instance)[(int)result];
				results.add(new SentimentResult(sentences.get(i).getPlainString(false),
						classification.getValue((int)result),result,confidence,instance.classValue(),
						sentences.get(i).getStartCharacter(),sentences.get(i).getEndCharacter(),sentences.get(i).getStart(),sentences.get(i).getEnd()));
			} catch (Exception e) {
				System.err.println("[OpinionPredictionRunner.classify] error classifying: " + e);
				e.printStackTrace();
			}
		}
		return results;
	}
	
    public void printResults(String output, String arffFile, String testType, ArrayList<SentimentResult> results, long time) {
    	try {
    		System.out.println("Sending Results to: " + output);
	    	BufferedWriter out = new BufferedWriter(new FileWriter(output + "/results.txt",true));
	    	BufferedWriter statout = new BufferedWriter(new FileWriter(output + "/statistical-significance.txt",true));
	    	
	    	double [] averages = {0,0,0,0,0,0,0,0,0,0};
	    	
	    	statout.write(arffFile + ",");
	    	
	    	// file, time elapsed, % correct, % incorrect, correctly classified, incorrectly classified, precision, recall, f-measure, TP, FP, FN, TN 
	    	for (SentimentResult result : results) {
	    		
	    		
	    		// correct
	    		if (result.getTrueValue() == result.getClassValue()) {
	    			averages[0]++;
	    			statout.write("1,");
	    		}
	    		// incorrect
	    		else if (result.getTrueValue() != result.getClassValue()) {
	    			averages[1]++;
	    			statout.write("0,");
	    		}
	    		//statout.write(result.getClassValue() + ",");
	    		// confusion matrix - positive
	    		if (result.getTrueValue() == 0 && result.getClassValue() == 0)
	    			averages[2]++;
	    		if (result.getTrueValue() == 0 && result.getClassValue() != 0)
	    			averages[3]++;
	    		if (result.getTrueValue() != 0 && result.getClassValue() == 0)
	    			averages[4]++;
	    		if (result.getTrueValue() != 0 && result.getClassValue() != 0)
	    			averages[5]++;
	    		
	    		if (result.getTrueValue() == 1 && result.getClassValue() == 1)
	    			averages[6]++;
	    		if (result.getTrueValue() == 1 && result.getClassValue() != 1)
	    			averages[7]++;
	    		if (result.getTrueValue() != 1 && result.getClassValue() == 1)
	    			averages[8]++;
	    		if (result.getTrueValue() != 1 && result.getClassValue() != 1)
	    			averages[9]++;
	    	}
	    
	    	//out.write("file, time elapsed, % correct, % incorrect, TP, FP, FN, TN, precision, recall, f-measure\n");
	    	out.write(arffFile + "," + testType + "," + (int) ( time / (1000*60)));
	    	
	    	averages[0] /= results.size();
	    	averages[1] /= results.size();
	    	
	    	for (double average : averages) {
	    		out.write("," + average);
	    	}
	    	
	    	// positive f-score
	    	double precision = averages[2] / (averages[2] + averages[3]);
	    	double recall = averages[2] / (averages[2] + averages[4]); 
	    	double fPos = 2 * ((precision * recall) / (precision + recall));
	    	out.write("," + precision + "," + recall + "," + fPos);
	    	// negative f-score
	    	precision = averages[6] / (averages[6] + averages[7]);
	    	recall = averages[6] / (averages[6] + averages[8]); 
	    	double fNeg = 2 * ((precision * recall) / (precision + recall));
	    	out.write("," + precision + "," + recall + "," + fNeg);
	    	out.write("," + (fPos + fNeg)/2);
	    	
	    	statout.write("\n");
	    	statout.close();
	    	out.write("\n");
	    	out.close();
    	} catch (Exception e) {
    		System.err.println("[SentimentRunner.printResults]" + e);
    		e.printStackTrace();
    	}
    }
	
	private static boolean isChunked(String fileName) {
		String chunkedFileName = chunkDataFileName(fileName);
		return (new File(chunkedFileName).exists());
	}
	
	private static boolean isParsed(String fileName) {
		String parsedFileName = parseDataFileName(fileName);
		return (new File(parsedFileName).exists());
	}

	private static boolean isTagged(String fileName) {
		String taggedFileName = tagDataFileName(fileName);
		return (new File(taggedFileName).exists());
	}

	private static boolean isPreTagged(String fileName) {
		String preTaggedFileName = preTagDataFileName(fileName);
		return (new File(preTaggedFileName).exists());
	}
	
	private static boolean isPostTagged(String fileName) {
		String postTaggedFileName = postTagDataFileName(fileName);
		return (new File(postTaggedFileName).exists());
	}
	
	public static String preTagData(String dataFile) {
		try {
			dataFile = new SwapEmoticons("dictionary/emoticons.txt.codes").swapEmoticons(new File(dataFile), true,false);
			//dataFile = new Contractions("dictionary/contractions-parserversion.txt").swapContractions(new File(dataFile),dataFile + ".con");
			//dataFile = new Contractions("dictionary/contractions.txt").swapContractions(new File(dataFile),dataFile);
			NlpUtils.convertBracketsFile(dataFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dataFile;
	}

	public static String postTagData(String dataFile) {
		try {
			dataFile = new SwapEmoticons("dictionary/emoticons.txt.codes").swapEmoticons(new File(dataFile), false,true);
			NlpUtils.revertBracketsFile(dataFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dataFile;
	}
	
    public static String tagData(String dataFile) {
       // CRFTagger.main(new String[] {"-modeldir", "libs/CRFTagger/model/", "-inputfile", dataFile});
    	
    	if (_taggerModel == null) {
    		loadTagger();
    	}
    	
        _taggerData.readData(dataFile);
        _taggerData.cpGen(_taggerMaps.cpStr2Int);

        // inference
        _taggerModel.inferenceAll(_taggerData.getData());
        _taggerData.writeData(_taggerMaps.lbInt2Str, dataFile + ".pos");

        return tagDataFileName(dataFile);
    }
   
    public static void loadTagger() {
    	
    	_taggerMaps = new crf.tagger.Maps();
        _taggerDict = new crf.tagger.Dictionary();
        crf.tagger.FeatureGen taggerFGen = new crf.tagger.FeatureGen(_taggerMaps, _taggerDict);
        crf.tagger.Viterbi taggerVtb = new crf.tagger.Viterbi();
        _taggerData = new TaggingData();
        
        crf.tagger.Option taggerOpt = new crf.tagger.Option("libs/CRFTagger/model/");
        
        _taggerModel = new crf.tagger.Model(taggerOpt, _taggerMaps, _taggerDict, taggerFGen, taggerVtb);
        if (!_taggerModel.init()) {
            System.out.println("Couldn't load the model");
            System.out.println("Check the <model directory> and the <model file> again");
        }
    }

    public static String chunkData(String tagFile) {
    	System.out.println("Chunking: " + tagFile);
        //CRFChunker.main(new String[] {"-modeldir", "libs/CRFChunker/model/", "-inputfile", tagFile});
        
    	if (_chunkerModel == null) {
    		loadChunker();
    	}
    	
        _chunkerData.readData(tagFile);
        _chunkerData.cpGen(_chunkerMaps.cpStr2Int);

        // inference
        _chunkerModel.inferenceAll(_chunkerData.getData());
        _chunkerData.writeData(_chunkerMaps.lbInt2Str, tagFile + ".chk");

        return chunkDataFileName(tagFile);
    }
    
    public static void loadChunker() {
    	
    	_chunkerMaps = new crf.chunker.Maps();
        _chunkerDict = new crf.chunker.Dictionary();
        crf.chunker.FeatureGen chunkerFGen = new crf.chunker.FeatureGen(_chunkerMaps, _chunkerDict);
        crf.chunker.Viterbi chunkerVtb = new crf.chunker.Viterbi();
        _chunkerData = new ChunkingData();
        
        crf.chunker.Option chunkerOpt = new crf.chunker.Option("libs/CRFChunker/model/");
        
        _chunkerModel = new crf.chunker.Model(chunkerOpt, _chunkerMaps, _chunkerDict, chunkerFGen, chunkerVtb);
        if (!_chunkerModel.init()) {
            System.out.println("Couldn't load the model");
            System.out.println("Check the <model directory> and the <model file> again");
        }
    }

    public static String tagDataFileName(String dataFile) {
        return dataFile + ".pos";
    }
    
    public static String preTagDataFileName(String dataFile) {
        return dataFile + ".emo.con";
    }
    
    public static String postTagDataFileName(String dataFile) {
    	return dataFile + ".emo";
    }

    public static String chunkDataFileName(String tagFile) {
        return tagFile + ".chk";
    }
    
    public static String parseDataFileName(String tagFile) {
        return tagFile + ".parse";
    }
    
    public static String addParsingandPhrases(String file, int start) {
	
		List<String> lines = Utils.readLines(file);
		int count = 0;
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file + ".parse", start > 0 ? true : false));
			BufferedWriter phraseWriter = new BufferedWriter(new FileWriter(file + ".phrase", start > 0 ? true : false));
			
			for (String line : lines) {
				
				if (count < start) {
					count++;
					continue;
				}
				
				String [] sentences = (line.length() > 1 && line.startsWith("\"") && line.endsWith("\"")) ? line.substring(1,line.length()-1).split("\",\"") :
						line.split("\",\"");
				
				try {
					String output = "";
					
					for (String sentence : sentences) {
						//System.out.println(sentence.length());
						
						if (sentence.length() > 1000) {
							sentence = sentence.replaceAll("(\\.[\\s']+)","$1DELIM");
							String [] splitUP = sentence.split("DELIM");
							String o = "";
							for (String sub : splitUP) {
								if (sub.length() > 300) {
									String [] comma = sub.split(",");
									
									for (String c : comma) {
										if (c.trim().isEmpty()) continue;
										if (c.length() > 200) {
											int split = c.indexOf(" ", c.length()/2);
											String c1 = c.substring(0,split);
											String c2 = c.substring(split+1);
											
											Tree t = parse(c1);
											printPhraseTree(t,phraseWriter);
											o += t + " ";
											
											t = parse(c2);
											printPhraseTree(t,phraseWriter);
											o += t + " ";

											
										}
										else {
											Tree t = parse(c);
											printPhraseTree(t,phraseWriter);
											o += t + " ";
										}
									}
								}
								else {
									Tree t = parse(sub);
									printPhraseTree(t,phraseWriter);
									o += t + " ";
								}
							}
							output += "\"" + o + "\",";
							phraseWriter.write(" ");
						}
						else {
							Tree tree = parse(sentence);
							printPhraseTree(tree,phraseWriter);
							output += "\"" + tree + "\",";
							phraseWriter.write(" ");
						}
					}
					writer.write(output.substring(0,output.length()-1) + "\n");
					phraseWriter.write("\n");
					count++;
				} catch (Exception p) {
					System.err.println("[DataGenerator.addParsing] error parsing: " + line);
					p.printStackTrace();
				}

			    	//System.out.println();
		    }
			writer.close();
			phraseWriter.close();
		} catch (Exception e) {
	    	e.printStackTrace();
	    }
		return file + ".phrase";
    }
    
    private static Tree parse(String sentence) {
    	return (Tree) _lexicalizedParser.apply(
				_tokenizerFactory.getTokenizer(new StringReader(sentence)).tokenize());
    }
    
    private static void printPhraseTree(Tree tree, BufferedWriter writer) throws Exception {
    	
    	if (tree.size() <= 15) {
    		
    		int i = 0;
    		
    		for (Tree leaf : tree.getChildrenAsList()) {
    			String tag = "I";
    			
    			if (i == 0) tag = "B"; 
    			
    			for (Word word : leaf.yieldWords()) {
    				//CoreLabel word = (CoreLabel)w;
    				if (word.word().matches("\\p{Punct}+")) tag = "O";
    				writer.write(word.word().replaceAll("/", "|") + "/" + leaf.value().replaceAll("/","|") + "/" + tag  + "-" + leaf.value().replaceAll("/","|") + " ");
    				tag = "I";
    				i++;
    			}
    		}
    	}
    	else {
    		for (Tree t : tree.getChildrenAsList()) {
    			printPhraseTree(t,writer);
    		}
    	}
    }
	
    /** 
     * Use our own preprocessing to generate our own "chunks" ie phrases.
     * @param file
     */
    public static String preprocess(String fileName) {
    	return preprocess(fileName, true, true);
    }
    
    /** 
     * Use our own preprocessing to generate our own "chunks" ie phrases.
     * @param file
     */
    public static String preprocess(String fileName, boolean reprocess) {
    	return preprocess(fileName, true, reprocess);
    }
    
    /** 
     * Use our own preprocessing to generate our own "chunks" ie phrases.
     * @param file
     */
    public static String preprocess(String fileName, boolean parse, boolean reprocess) {
    	int size = processing.GeneralUtils.readLines(fileName).size();
    	
    	if (!reprocess && isPreTagged(fileName)) {
    		fileName = preTagDataFileName(fileName);
    	}
    	else {
    		fileName = preTagData(fileName);
    	}
		if (!reprocess && isTagged(fileName)) {
    		if (parse) tagDataFileName(fileName);
    		else fileName = tagDataFileName(fileName);
    	}
    	else {
    		tagData(fileName);
    	}
		
		if (parse && (reprocess || !isParsed(fileName) || processing.GeneralUtils.readLines(fileName + ".phrase").size() != size)) {
    		postTagData(addParsingandPhrases(fileName,new File(fileName + ".phrase").exists() && !reprocess ? processing.GeneralUtils.readLines(fileName + ".phrase").size() : 0));
    	}
		
		//if (parse) {
		if (isTagged(fileName))
			fileName = tagDataFileName(fileName);
		//}
		
    	if (!reprocess && isChunked(fileName) && processing.GeneralUtils.readLines(fileName + ".chk").size() == size) {
    		fileName = chunkDataFileName(fileName);
    	}
    	else {
    		fileName = chunkData(fileName);
    	}
    	
    	if (!reprocess && isPostTagged(fileName)) {
    		fileName = postTagDataFileName(fileName);
    	}
    	else {
    		fileName = postTagData(fileName);
    	}
    	return fileName;
    }
}
