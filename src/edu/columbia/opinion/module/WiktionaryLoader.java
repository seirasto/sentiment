package edu.columbia.opinion.module;
import java.util.Properties;

import processing.GeneralUtils;

import wiktionary.Wiktionary;

/**
 * 
 */

/**
 * @author sara
 *
 */
public class WiktionaryLoader {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String directory = "";
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			directory = prop.getProperty("wiktionary_directory");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String input = args[0];
		String output = directory + "/wiktionary/";
		
		Wiktionary.createDatabase(input, output);
		
		System.out.println("--- Test Database ---");
		
		Wiktionary wiktionary = new Wiktionary(directory);
		System.out.println(wiktionary.query("hello"));
		System.out.println("----------");
		System.out.println(wiktionary.query("lol").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("LOL").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("orgasmic"));
		System.out.println("----------");
		System.out.println(wiktionary.query("'s","Suffix"));
		System.out.println("----------");
		System.out.println(wiktionary.query("doesnt","Verb").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("tonite","Noun").get(0).getFormsOf());
	}
}
