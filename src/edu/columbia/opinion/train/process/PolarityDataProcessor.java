package edu.columbia.opinion.train.process;

import java.util.ArrayList;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import edu.columbia.opinion.classifications.Classification;
import edu.columbia.opinion.io.*;
import edu.columbia.opinion.process.*;

public class PolarityDataProcessor extends TrainDataProcessor {
	
    public PolarityDataProcessor(Classification classification, AbstractDataReader trainReader, String outputFile, 
    		int nGramAmount, boolean overwrite, boolean socialMedia, boolean sentiWordNet, boolean normalized, 
    		boolean tuneToAnnotations) {
		super(classification, trainReader, outputFile, nGramAmount, overwrite, socialMedia, sentiWordNet, normalized,tuneToAnnotations);
	}
    
    public PolarityDataProcessor(Classification classification, String modelFile, int nGramSize) {
		super(classification, modelFile, nGramSize);
	}
    
	protected ArrayList<Attribute> getAttributes() {
		return getAttributesPolarity();
	}
 	
	protected Instance sentenceToInstance(Sentence sentence, Instances dataset) {
        return sentenceToInstancePolarity(sentence, dataset);
    }
}
