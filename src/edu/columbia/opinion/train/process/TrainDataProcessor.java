package edu.columbia.opinion.train.process;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.File;
import edu.columbia.opinion.io.*;
import edu.columbia.opinion.process.*;
import edu.columbia.opinion.classifications.*;
import java.util.*;

import social_media.Features;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.*;
import weka.core.converters.ArffSaver;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.filters.supervised.attribute.AttributeSelection;


public abstract class TrainDataProcessor extends DataProcessor {

	protected boolean _overwrite;
	
    protected Instances _train;
    protected List<Sentence> _trainSentences;
    
    protected Instances _trainUntouched;    
    protected ArrayList<Attribute> _attVector;

	protected StringToWordVector _textNgramFilter;
	protected StringToWordVector _polarChunkNgramFilter;
	protected StringToWordVector _smFilter;
	protected StringToWordVector _counterFilt;
	protected AttributeSelection _chiSquareFilter;
	HashSet<String> _nGrams;
	HashSet<String> _chunkGrams;
	HashSet<String> _sm;
    
    public TrainDataProcessor(Classification classification, AbstractDataReader trainReader, String outputFile, int nGramSize) {
    	this(classification,trainReader,outputFile,nGramSize,false,true,true, true,true);
    }
    
    public TrainDataProcessor(Classification classification, AbstractDataReader trainReader, String outputFile, int nGramSize, boolean overwrite) {
    	this(classification,trainReader,outputFile,nGramSize,overwrite,true,true,true, true);
    }
    
    public TrainDataProcessor(Classification classification, String modelFile, int nGramSize) {
    	this(classification,null, modelFile,nGramSize,false,false, true,true,true);
    }

    public TrainDataProcessor(Classification classification, AbstractDataReader trainReader, String outputFile, int nGramSize, boolean overwrite, boolean socialMedia, boolean sentiWordNet, boolean normalized, boolean tuneToAnnotations) {
    	super(classification,socialMedia,sentiWordNet,nGramSize, tuneToAnnotations);
		_outputFile = outputFile;
		_overwrite = overwrite;
		_normalized = normalized;
		makeTrainDataset(trainReader);
	}
	
	public Instances getTrainSet() {
		_train.setClass(_train.attribute(getClassName()));
		return _train;
	}
	
	protected void makeTrainDataset(AbstractDataReader trainReader) {
		ArrayList<Attribute> attVector = getAttributes();
		
		try {
			// already created
			if (new File(_outputFile).exists() && !_overwrite) {
				System.out.println("Loading: " + _outputFile);
				//createFilters(new ArffReader(_outputFile).getInstances());
				_train = new ArffReader(_outputFile).getInstances();
			}
			else {
				System.out.println("Creating: " + _outputFile);
				_train = getBaseInstances("Train set", attVector, trainReader, _trainSentences = new ArrayList<Sentence>());

		    	_train.setClassIndex(0);
		    	_train = removeDuplicates(_train);

				// save instances so that it doesn't have to be generated from scratch again
		    	if (!new File(_outputFile).exists() || _overwrite) {
			    	ArffSaver saver = new ArffSaver();
			    	saver.setInstances(_train);
			    	saver.setFile(new File(_outputFile));
			    	saver.writeBatch();
		    	}
		    	//createFilters(train);
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected Instances getBaseInstances(String name, ArrayList<Attribute> attVector, AbstractDataReader reader, List<Sentence> sentenceList) {
        Instances instances = new Instances(name, attVector, 0);
                
        for (Sentence sentence : reader.getSentences()) {
            Instance instance = sentenceToInstance(sentence, instances);
            if(instance == null)
            	{
            	  continue;
            	}
            
            if (sentence.getClassification() != null) {
            	instance.setValue(instances.attribute(getClassName()), sentence.getClassification());
            }
            
            instances.add(instance);
            sentenceList.add(sentence);
        }
        //System.exit(0); // when computing sm stats
        return instances;
	}
	
	/**
	 * If data point is appearing as subjective and objective, remove the objective 
	 * data point as we are interested in finding chunks that have subjectivity in them.
	 *  
	 * @param instances
	 * @return
	 */
	public Instances removeDuplicates(Instances instances) {
	
		for (int index = 1; index < instances.numInstances(); index++) {
			Instance thisInstance = instances.instance(index);
			Instance lastInstance = instances.instance(index-1);
			
			if (thisInstance.stringValue(instances.attribute(TEXT_ATT)).equals(lastInstance.stringValue(instances.attribute(TEXT_ATT)))) {
				if(instances.instance(index).classValue() == 1) instances.delete(index);
				else instances.delete(index-1);
				index -= 1;
			}
		}
		System.out.println("Reduced # of instances to " + instances.numInstances());
		return instances;
	}
	
	public Instances createFilters(Instances train) throws Exception {
		
		int size = train.numAttributes();
		train.setClass(train.attribute(getClassName()));
		
		if (_nGramSize == 0) {
		
			train.deleteAttributeAt(train.attribute(POLAR_GRAM_TEXT_ATT).index());
			train.deleteAttributeAt(train.attribute(TEXT_ATT).index());
			size = train.numAttributes();

			// apply feature selection on all features that start with "SM_"
			/*if (_socialMedia) {
				train = chiSquareByName(train,"SM_");
		        _sm = setNgramsByName(train,"SM_");
		        
		        for (String sm : _sm) {
		        	System.out.print("\"" + sm.substring(3).replaceAll("\"", "\"\"") + "\",");
		        }
		        System.out.println();
			}*/
			
			/*if (_socialMedia) {
			       // generate sm counts
		        _smFilter = getNgramFilter(
		        		train, 
		        		train.attribute(SM_TEXT).index(),200,1,1);
		        train = Filter.useFilter(train, _smFilter);
		        train = adjustAttributeNames("SM",train,size-1,train.numAttributes());
		        train = chiSquare(train,size,train.numAttributes());
		        
		        _sm = setNgrams(train,size,train.numAttributes());
		        
		        System.out.print("SM Features: ");
		        
		        for (String sm : _sm) {
		        	System.out.print("\"" + sm.substring(3).replaceAll("\"", "\"\"") + "\",");
		        }
		        System.out.println();
		        //System.out.println(_sm); //less file.in | grep "\SM" | sed 's/SM_//g' | sed 's/^\[//g' | sed 's/\]$//g' | cat file.out
		        size = train.numAttributes();
			}*/
			// generate POS counts
	        _counterFilt = getCounterFilter(train, train.attribute(TAG_TEXT_ATT).index());
	        train = Filter.useFilter(train, _counterFilt);
	        train = adjustAttributeNames("POS",train,size-1,train.numAttributes());
	        
	        // put class last
	        train = reorder(train);
	        
	        //_train = train; 
	        return train;
		}
		
        // generate polar chunk n-grams
        _polarChunkNgramFilter = getNgramFilter(
        		train, 
        		train.attribute(POLAR_GRAM_TEXT_ATT).index(),
        		/*wordsToKeep*/ 100, 
        		/*ngramMinSize*/ 1, 
        		/*ngramMaxSize*/ 3);
        train = Filter.useFilter(train, _polarChunkNgramFilter);
        train = adjustAttributeNames("PC",train,size-1,train.numAttributes());
        train = chiSquare(train,size,train.numAttributes());
        _chunkGrams = setNgrams(train,size,train.numAttributes());
        size = train.numAttributes();
        
        // generate text n-grams
        _textNgramFilter = getNgramFilter(
        		train, 
        		train.attribute(TEXT_ATT).index(), 
        		/*wordsToKeep*/ _nGramSize, 
        		/*ngramMinSize*/ 1, 
        		/*ngramMaxSize*/ 3);
        train = Filter.useFilter(train, _textNgramFilter);
        train = adjustAttributeNames("NG",train,size-1,train.numAttributes());
        train = chiSquare(train,size,train.numAttributes());
        _nGrams = setNgrams(train,size,train.numAttributes());
        size = train.numAttributes();
        
        // generate sm counts
        /*if (_socialMedia) {
	        _smFilter = getNgramFilter(
	        		train, 
	        		train.attribute(SM_TEXT).index(),_nGramSize,1,1);
	        train = Filter.useFilter(train, _smFilter);
	        train = adjustAttributeNames("SM",train,size-1,train.numAttributes());
	        train = chiSquare(train,size,train.numAttributes());
	        _sm = setNgrams(train,size,train.numAttributes());
	        size = train.numAttributes();
        }*/

        // generate POS counts
        _counterFilt = getCounterFilter(train, train.attribute(TAG_TEXT_ATT).index());
        train = Filter.useFilter(train, _counterFilt);
        train = adjustAttributeNames("POS",train,size-1,train.numAttributes());
        
        /*if (_socialMedia) {
			train = chiSquareByName(train,"SM_");
	        _sm = setNgramsByName(train,"SM_");
	        
	        for (String sm : _sm) {
	        	System.out.print("\"" + sm.substring(3).replaceAll("\"", "\"\"") + "\",");
	        }
	        System.out.println();
		}*/
        
        // put class last
        train = reorder(train);
        
        return train;
        //_train = train; 
	}
	
	public void printStatistics() {
        
        for (Sentence sentence : this._trainSentences) {
        	
        	Hashtable<String,Double> features = _socialMediaGenerator.compute(sentence.getPlainStringArray(true));

        	double numWords = features.get(Features.NUM_WORDS);
	    	
        //  Num Words, Capital Words, %, Slang, %, :), %, LOL, %, ., %, .+$#@, %, Punctuation Count, %, !, %, 
        	//  !!!, %, ?, %, ???, %, ..., %, LINKS, %, sweeeet, %, All Caps, %, avg word length 
        	
        	System.out.println(features.get(Features.NUM_WORDS) 
        		+ "," + features.get(Features.CAPITAL_WORDS) + "," + features.get(Features.CAPITAL_WORDS)/numWords
        		+ "," + features.get(Features.SLANG) + "," + features.get(Features.SLANG)/numWords
        		+ "," + features.get(Features.EMOTICONS) + "," + features.get(Features.EMOTICONS)/numWords
        		+ "," + features.get(Features.ACRONYMS) + "," + features.get(Features.ACRONYMS)/numWords
        		+ "," + features.get(Features.PUNCTUATION) + "," + features.get(Features.PUNCTUATION)/numWords
        		+ "," + features.get(Features.PUNCTUATION_R) + "," + features.get(Features.PUNCTUATION_R)/numWords
        		+ "," + features.get(Features.PUNCTUATION_NUM) + "," + features.get(Features.PUNCTUATION_NUM)/numWords
        		+ "," + features.get(Features.EXCLAMATION) + "," + features.get(Features.EXCLAMATION)/numWords
        		+ "," + features.get(Features.EXCLAMATION_R) + "," + features.get(Features.EXCLAMATION_R)/numWords
        		+ "," + features.get(Features.QUESTION) + "," + features.get(Features.QUESTION)/numWords
        		+ "," + features.get(Features.QUESTION_R) + "," + features.get(Features.QUESTION_R)/numWords
        		+ "," + features.get(Features.ELLIPSES) + "," + features.get(Features.ELLIPSES)/numWords
        		+ "," + features.get(Features.LINKS) + "," + features.get(Features.LINKS)/numWords
        		+ "," + features.get(Features.WORD_LENGTHENING) + "," + features.get(Features.WORD_LENGTHENING)/numWords
        		+ "," + features.get(Features.ALL_CAPS_WORDS) + "," + features.get(Features.ALL_CAPS_WORDS)/numWords
        		+ "," + features.get(Features.AVG_WORD_LENGTH)
        		+ "," + (sentence.getClassification()));
        }
	}

	protected abstract Instance sentenceToInstance(Sentence sentence, Instances instances);
	
	protected Instances chiSquare(Instances instances, int start, int count) throws Exception {
		// 1. Copy instances in list
		Remove chiSquareFilt = new Remove();
        
        // set class to be included
        chiSquareFilt.setAttributeIndices("first," + start + "-" + count);
        chiSquareFilt.setInvertSelection(true);
        chiSquareFilt.setInputFormat(instances);
        Instances subset = Filter.useFilter(instances, chiSquareFilt);
        subset.setClassIndex(0);
		// 2. Perform Chi Square Filter
        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
		eval.buildEvaluator(subset);
		
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		for (int index = 1; index < subset.numAttributes(); index++) {
			if (eval.evaluateAttribute(index) == 0.0) {
			    //System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
				instances.deleteAttributeAt(instances.attribute(subset.attribute(index).name()).index());
				deleted++;
			}
			else kept++;
		}
		//System.err.println();
		System.out.println("[DataProcessor.chiSquare] removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		return instances;
	}
	
	protected Instances chiSquareByName(Instances instances, String name) throws Exception {
		// 1. Copy instances in list
		Remove chiSquareFilt = new Remove();
        
		String indices = "first";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				indices += "," + (index+1);
			}
		}
		
        // set class to be included
        chiSquareFilt.setAttributeIndices(indices);
        chiSquareFilt.setInvertSelection(true);
        chiSquareFilt.setInputFormat(instances);
        Instances subset = Filter.useFilter(instances, chiSquareFilt);
        subset.setClassIndex(0);
		// 2. Perform Chi Square Filter
        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
		eval.buildEvaluator(subset);
		
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		for (int index = 1; index < subset.numAttributes(); index++) {
			if (eval.evaluateAttribute(index) == 0.0) {
			    //System.err.print(subset.attribute(index) + ": " + eval.evaluateAttribute(index) + ", ");
				instances.deleteAttributeAt(instances.attribute(subset.attribute(index).name()).index());
				deleted++;
			}
			else kept++;
		}
		//System.err.println();
		System.out.println("[DataProcessor.chiSquare] removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		return instances;
	}
	
	protected HashSet<String> setNgramsByName(Instances instances, String name) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				grams.add(instances.attribute(index).name());
			}
		}
		return grams;
	}

	protected HashSet<String> setNgrams(Instances instances, int start, int end) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = start; index < end; index++) {
			grams.add(instances.attribute(index).name());
		}
		return grams;
	}
	
	protected abstract ArrayList<Attribute> getAttributes();
	
	public List<Sentence> getTrainSentences() {
		return _trainSentences;
	}
}
