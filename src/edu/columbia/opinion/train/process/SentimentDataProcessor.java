package edu.columbia.opinion.train.process;

import java.util.ArrayList;

import edu.columbia.opinion.classifications.Classification;
import edu.columbia.opinion.io.AbstractDataReader;
import edu.columbia.opinion.process.*;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class SentimentDataProcessor extends TrainDataProcessor {

    public SentimentDataProcessor(Classification classification, AbstractDataReader trainReader, String outputDirectory, int nGramAmount, 
    		boolean overwrite, boolean socialMedia, boolean sentiWordNet, boolean normalized, boolean tuneToAnnotations) {
		super(classification, trainReader, outputDirectory, nGramAmount,overwrite,socialMedia, sentiWordNet, normalized,tuneToAnnotations);
	}
	
    public SentimentDataProcessor(Classification classification, AbstractDataReader trainReader, String outputDirectory, int nGramAmount, boolean overwrite) {
		super(classification, trainReader, outputDirectory, nGramAmount,overwrite);
	}
    
    public SentimentDataProcessor(Classification classification, AbstractDataReader trainReader, String outputDirectory, int nGramAmount) {
		super(classification, trainReader, outputDirectory, nGramAmount);
	}

    public SentimentDataProcessor(Classification classification, String modelFile, int nGramSize) {
		super(classification, modelFile, nGramSize);
	}
	
	protected ArrayList<Attribute> getAttributes() {
			return getAttributesSubjectivity();
	}
 	
	protected Instance sentenceToInstance(Sentence sentence, Instances dataset) {
        return sentenceToInstanceSubjectivity(sentence, dataset);
    }
}
