/**
 * 
 */
package edu.columbia.opinion.train.module;

import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.train.io.*;
import edu.columbia.opinion.train.process.*;
import edu.columbia.opinion.module.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SMO;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.Classifier;
import weka.filters.supervised.instance.Resample;
/**
 * @author sara
 *
 */
public abstract class SentimentRunner {
	
	TrainDataProcessor _processor;
	Classification _classification;
	String _classifier = "-logistic";
	public boolean _useEmoticons;
	public boolean _useWiktionary;
	public boolean _useWordNet;
	public boolean _socialMedia;
	public String _trainingFile;
	public int _type;
	public String _useNoun;
	
	public AbstractDataReader getReader(List<String> sentences) {
		return new UnseenDataReader(sentences,_type,_useWordNet,_useWiktionary,_useEmoticons,_useNoun);
	}
	
	public AbstractDataReader getReader(List<String> sentences, boolean chunked) {
		return new UnseenDataReader(sentences,_type,_useWordNet,_useWiktionary,_useEmoticons,_useNoun, chunked);
	}
	
	public AbstractDataReader getReader(List<String> processedSentences, List<String> originalSentences, List<String> annotation) {		
		return new AnnotatedDataReader(false,_classification, processedSentences, originalSentences,annotation,_useWordNet,_useWiktionary,_useEmoticons,false,_useNoun);
	}
	
	public AbstractDataReader getReader(String file, int type, boolean chunked) {
		return new UnseenDataReader(file,type,_useWordNet,_useWiktionary,_useEmoticons,_useNoun,chunked);
	}
	
	public AbstractDataReader getReader(List<String> files, String directory) {
		
		List<String> processed = new ArrayList<String>();
		List<String> text = new ArrayList<String>();
		List<String> annotation = new ArrayList<String>();
		
		for (int index = 0; index < files.size(); index++) {
			text.add(directory + "/" + files.get(index) + "_text.txt");
			annotation.add(directory + "/" + files.get(index) + "_annotation.txt");
			processed.add(OpinionRunner.preprocess(directory + "/" + files.get(index) + "_text.txt"));
		}		
		return getReader(processed, text, annotation); 
	}
	
	public String buildANDsaveModel(String directory, boolean balanced, double ridge) throws Exception {
		
		System.out.println("[SentimentRunner.BuildAndSaveModel " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Load training set");
		Instances train = _processor.getTrainSet();
		train.setClassIndex(0);

		
		if (balanced) {
			System.out.println("[SentimentRunner.BuildAndSaveModel " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Balance training set");
			train = balance(train);
			System.out.println("SentimentRunner.BuildAndSaveModel " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] " + Arrays.toString(train.attributeStats(train.classIndex()).nominalCounts));
		}

		System.out.println("[SentimentRunner.BuildAndSaveModel " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Filter and generate N-grams");
		train = _processor.createFilters(train);
		
		// 1. save attributes to file (not data)
		ArffSaver saver = new ArffSaver();
		//train.delete();
		saver.setInstances(train);
	    saver.setFile(new File(directory + _classification.getName() + ".arff"));
	    saver.writeBatch();
		
		Classifier classifier = null;
		
		if (_classifier.equals("-logistic")) {
			classifier = new Logistic();
			if (ridge != -1) ((Logistic)classifier).setRidge(ridge);
		}
		else if (_classifier.equals("-svm")) {
			classifier = new SMO();
		}
		else if (_classifier.equals("-nb")) {
			classifier = new NaiveBayes();
		}
    	
		System.out.println("[SentimentRunner.BuildAndSaveModel " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Classifying using " + _classifier);
		classifier.buildClassifier(train);
		
		// 2. save model
		System.out.println("[SentimentRunner.BuildAndSaveModel " + 
				new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Saving model to: " + directory + _classification.getName() + ".model");
		new File(directory + _classification.getName() + ".model").getParentFile().mkdirs();
		ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(directory + _classification.getName() + ".model"));
		oos.writeObject(classifier);
		oos.flush();
		oos.close();
	    
	    return directory + _classification.getName();
	}
	
	private Instances balance(Instances instances) throws Exception {
		
		Resample balance = new Resample();
		balance.setInputFormat(instances);
		int min = instances.attributeStats(instances.classIndex()).nominalCounts[0];
		
		for (int index = 1; index < instances.numClasses(); index++) {
			if (instances.attributeStats(instances.classIndex()).nominalCounts[index] < min) 
				min = instances.attributeStats(instances.classIndex()).nominalCounts[index];
		}

		double percent = min/(double)instances.numInstances();
		balance.setSampleSizePercent(Math.ceil(percent*100*instances.numClasses()));

		balance.setNoReplacement(true);
		balance.setBiasToUniformClass(1);
		return Resample.useFilter(instances, balance);
	
	}
	
    public void printStats() {
    	_processor.printStatistics();
    }

}
