package edu.columbia.opinion.train.module;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

import edu.columbia.opinion.io.AbstractDataReader;
import edu.columbia.opinion.io.SentimentResult;
import edu.columbia.opinion.module.OpinionRunner;
import edu.columbia.opinion.process.TestDataProcessor;
import edu.columbia.opinion.train.process.PolarityDataProcessor;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.train.io.*;

public class PolarityPredictionRunner extends SentimentRunner {
	
	boolean _neutral = false;
	
	public static void main(String[] args) throws Exception {
			
		List<String> trainingType = Arrays.asList("lj","mturk","mpqa");
		String testingType = "lj_test+mturk_test+mpqa_test";
		String trainingDirectory = "/proj/nlp/users/sara/java/sentiment/data/coling/polarity/";
		String outputDirectory = "/proj/nlp/users/sara/sentiment/coling/";
		boolean useWordnet = false;
		boolean useWiktionary = false;
		boolean useEmoticons = false;
		boolean balanced = false;
		boolean socialMedia = false;
		boolean normalized = true;
		boolean neutral = false;
		boolean tuneToAnnotations = false;
		String useNoun = "";
		String outputFile = "train";
		int nGramAmount = 1000;
		double ridge = -1;
		String classifier = "-logistic";
		String model = null;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-i")) {
				trainingDirectory = args[++i];
			}
			else if (args[i].equals("-o")) {
				outputDirectory = args[++i];
			}
			else if (args[i].equals("-t")) {
				trainingType = Arrays.asList(args[++i].split("\\+"));
				outputFile += "-" + args[i];
			}
			else if (args[i].equals("-test")) {
				testingType = args[++i];
			}
			else if (args[i].equals("-wiktionary") || args[i].equals("-wi")) {
				useWiktionary = true;
				outputFile += "-wi";
			}
			else if (args[i].equals("-wordnet") || args[i].equals("-wo")) {
				useWordnet = true;
				outputFile += "-wo";
			}
			else if (args[i].equals("-emoticons") || args[i].equals("-em")) {
				useEmoticons = true;
				outputFile += "-em";
			}
			else if (args[i].equals("-neutral")) {
				neutral = true;
				outputFile += "-3way";
			}
			else if (args[i].equals("-sm")) {
				socialMedia = true;
				outputFile += "-sm";
			}
			else if (args[i].equals("-balanced")) {
				balanced = true;
				outputFile += "-bal";
			}
			else if (args[i].equals("-n")) {
				nGramAmount = Integer.valueOf(args[++i]);
				outputFile += "-n" + nGramAmount;
			}
			else if (args[i].equals("-nn")) {
				useNoun = "NN";
				outputFile += "-nn";
			}
			else if (args[i].equals("-nnp")) {
				useNoun = "NNP";
				outputFile += "-nnp";
			}
			else if (args[i].equals("-nonorm")) {
				normalized = false;
			}

			else if (args[i].equals("-logistic")) {
				classifier = args[i];
				outputFile += "-logistic";
			}
			else if (args[i].equals("-svm")) {
				classifier = args[i];
				outputFile += "-svm";
			}
			else if (args[i].equals("-nb")) {
				classifier = args[i];
				outputFile += "-naivebayes";
			}
			else if (args[i].equals("-model")) {
				model = args[++i];
			}
			else if (args[i].equals("-2real")) {
				tuneToAnnotations = false;
			}
 			else {
				System.out.println("Invalid arguments: \"" + args[i] + 
						"\"\n sentiment.app.OpinionPredictionRunner -o outputDirectory -i inputDirectory -t (lj+mpqa+mturk) -wordnet -wiktionary -emoticons");
				System.exit(0);
			}
		}	
		
		new File(outputDirectory + "/logs/").mkdir();
		new File(outputDirectory + "/output/").mkdir();
		System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(outputDirectory + "/logs/" + outputFile + ".log")),true)); 
    	System.out.println( "=====================================================================================\n" +
    			outputFile +
    						"\n=====================================================================================");
    	System.err.println("System.out is being sent to: " + outputDirectory + "/logs/" + outputFile + ".log");
    	
    	PolarityPredictionRunner p = null;
    	
    	p = new PolarityPredictionRunner(outputDirectory + "/output/" + outputFile + ".arff",useWordnet,useWiktionary,useEmoticons,
    			socialMedia, useNoun, neutral, classifier);
		
    	
    	if (model == null) {	
    		p.train(trainingType, trainingDirectory + "/train/", outputDirectory + "/output/" + outputFile + ".arff", balanced, nGramAmount, true, normalized, tuneToAnnotations);	
			model = p.buildANDsaveModel(outputDirectory + "/model/" + outputFile + "-", balanced, ridge);
    	}
    	
		if (testingType != null) {
			OpinionRunner runner = new OpinionRunner(useWordnet,useWiktionary,useEmoticons, socialMedia, nGramAmount, tuneToAnnotations);
			long startTime = System.currentTimeMillis();
			
			TestDataProcessor processor = new edu.columbia.opinion.process.PolarityDataProcessor(
					new PolarityClassification(neutral),socialMedia,useWiktionary,nGramAmount,tuneToAnnotations);
			AbstractDataReader reader = p.getReader(Arrays.asList(testingType.split("\\+")), trainingDirectory + "/test/", false);
    		
			System.out.println("Experimenting using Model: " + model);
	    	ArrayList<SentimentResult> results = runner.test(model, processor, reader);
			runner.printResults(outputDirectory + "/output/", outputFile, testingType, results, System.currentTimeMillis() - startTime);
    	}
    	
    	System.setOut(System.out);
	}
	
	public PolarityPredictionRunner(String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, 
			boolean socialMedia, String useNoun, boolean neutral, String classifier) {
		
		
		_type = Classification.POLARITY;
		_useWordNet = useWordNet;
		_useWiktionary = useWiktionary;
		_useEmoticons = useEmoticons;
		_socialMedia = socialMedia;
		_trainingFile = outputFile;
		_useNoun = useNoun;
		_neutral = neutral;
		_classification = new PolarityClassification(_neutral);		
		_classifier = classifier;
	}
	
	public void train(List<String> trainingTypes, String trainingDirectory, String outputFile, boolean balanced, int nGramAmount, boolean overwrite, boolean normalized, boolean tuneToAnnotations) {
		_processor = new PolarityDataProcessor(
				_classification,
				getReader(trainingTypes,trainingDirectory, balanced),
						outputFile,nGramAmount,overwrite,_socialMedia,_useWiktionary,normalized,tuneToAnnotations);
		printStats();
		train();
	}
	
	public AnnotatedPolarityDataReader getReader(List<String> type, String directory, boolean balanced) {
		
		List<String> files = type;
		List<String> processed = new ArrayList<String>();
		List<String> text = new ArrayList<String>();
		List<String> annotation = new ArrayList<String>();
		
		for (int index = 0; index < files.size(); index++) {
			System.out.println("[PolarityPredictionRunner " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Processing " + files.get(index));
			text.add(directory + "/" + files.get(index) + "_text.txt");
			annotation.add(directory + "/" + files.get(index) + "_annotation.txt");
			processed.add(OpinionRunner.preprocess(directory + "/" + files.get(index) + "_text.txt",false, false));
		}
		return new AnnotatedPolarityDataReader(false,_classification, processed,text,annotation
				,_useWordNet,_useWiktionary,_useEmoticons,balanced,_useNoun,_neutral);
	}
		
	private void train() {
		_processor.getTrainSet();
	}
}
