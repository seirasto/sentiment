package edu.columbia.opinion.train.module;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.io.*;

import edu.columbia.opinion.train.process.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.train.io.*;
import edu.columbia.opinion.io.AbstractDataReader;
import edu.columbia.opinion.io.SentimentResult;
import edu.columbia.opinion.module.*;
import edu.columbia.opinion.process.SubjectivityDataProcessor;
import edu.columbia.opinion.process.TestDataProcessor;

public class SubjectivityPredictionRunner extends SentimentRunner {

	public static void main(String[] args) throws Exception {
		
		List<String> trainingType = Arrays.asList("lj","mturk","mpqa");
		String testingType = "lj_test+mturk_test+mpqa_test";
		String trainingDirectory = "/proj/nlp/users/sara/java/sentiment/data/coling/subjective/";
		String outputDirectory = "/proj/nlp/users/sara/sentiment/coling/";
		String classifier = "-logistic";
		boolean useWordnet = false;
		boolean useWiktionary = false;
		boolean useEmoticons = false;
		boolean balanced = false;
		boolean socialMedia = false;
		boolean normalized = true;
		boolean tuneToAnnotations = true;
		String useNoun = "";
		String outputFile = "train";
		String model = null;
		int nGramAmount = 1000;
		double ridge = -1;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-i")) {
				trainingDirectory = args[++i];
			}
			else if (args[i].equals("-o")) {
				outputDirectory = args[++i];
			}
			else if (args[i].equals("-t")) {
				trainingType = Arrays.asList(args[++i].split("\\+"));
				outputFile += "-" + args[i];
			}
			else if (args[i].equals("-test")) {
				testingType = args[++i];
			}
			else if (args[i].equals("-wiktionary") || args[i].equals("-wi")) {
				useWiktionary = true;
				outputFile += "-wi";
			}
			else if (args[i].equals("-wordnet") || args[i].equals("-wo")) {
				useWordnet = true;
				outputFile += "-wo";
			}
			else if (args[i].equals("-emoticons") || args[i].equals("-em")) {
				useEmoticons = true;
				outputFile += "-em";
			}
			else if (args[i].equals("-sm")) {
				socialMedia = true;
				outputFile += "-sm";
			}
			else if (args[i].equals("-balanced")) {
				balanced = true;
				outputFile += "-bal";
			}
			else if (args[i].equals("-n")) {
				nGramAmount = Integer.valueOf(args[++i]);
				outputFile += "-n" + nGramAmount;
			}
			else if (args[i].equals("-nn")) {
				useNoun = "NN";
				outputFile += "-nn";
			}
			else if (args[i].equals("-nnp")) {
				useNoun = "NNP";
				outputFile += "-nnp";
			}
			else if (args[i].equals("-nonorm")) {
				normalized = false;
			}
			else if (args[i].equals("-logistic")) {
				classifier = args[i];
				outputFile += "-logistic";
			}
			else if (args[i].equals("-svm")) {
				classifier = args[i];
				outputFile += "-svm";
			}
			else if (args[i].equals("-nb")) {
				classifier = args[i];
				outputFile += "-naivebayes";
			}
			else if (args[i].equals("-model")) {
				model = args[++i];
			}
			else if (args[i].equals("-2real")) {
				tuneToAnnotations = false;
			}
 			else {
				System.out.println("Invalid arguments: \"" + args[i] + 
						"\"\n sentiment.app.OpinionPredictionRunner -o outputDirectory -i inputDirectory -t (lj+mpqa+mturk) -wordnet -wiktionary -emoticons");
				System.exit(0);
			}
		}	
		
		new File(outputDirectory + "/logs/").mkdirs();
		System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(outputDirectory + "/logs/" + outputFile + ".log")),true)); 
    	System.out.println( "=====================================================================================\n" +
    			outputFile +
    						"\n=====================================================================================");
    	System.err.println("System.out is being sent to: " + outputDirectory + "/logs/" + outputFile + ".log");
    	
    	
    	SubjectivityPredictionRunner s = null;
    	if (model == null) {	
			s = new SubjectivityPredictionRunner(trainingType,trainingDirectory + "/train/",
	    			outputDirectory + "/output/" + outputFile + ".arff",useWordnet,useWiktionary,useEmoticons,balanced, nGramAmount, true, socialMedia, useNoun, normalized, classifier, tuneToAnnotations);
			model = s.buildANDsaveModel(outputDirectory + "/model/" + outputFile + "-", balanced, ridge);
    	}
    	
		if (testingType != null) {
			OpinionRunnerInternal runner = new OpinionRunnerInternal(useWordnet,useWiktionary,useEmoticons, socialMedia, nGramAmount, tuneToAnnotations);
			long startTime = System.currentTimeMillis();
			
			TestDataProcessor processor = new SubjectivityDataProcessor(new OpinionClassification(),socialMedia,useWiktionary,nGramAmount,tuneToAnnotations);
			AbstractDataReader reader = runner.getReader(Arrays.asList(testingType.split("\\+")),trainingDirectory + "/test/", new  OpinionClassification());
    		
	    	ArrayList<SentimentResult> results = runner.test(model, processor, reader);
			runner.printResults(outputDirectory + "/output/", outputFile, testingType, results, System.currentTimeMillis() - startTime);
    	}
    	
    	System.setOut(System.out);
	}
	
	protected SubjectivityPredictionRunner( String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, int nGramAmount, boolean overwrite, boolean socialMedia, String useNoun, String classifier) {
		_type = Classification.SUBJECTIVITY;
		_useWordNet = useWordNet;
		_useWiktionary = useWiktionary;
		_useEmoticons = useEmoticons;
		_socialMedia = socialMedia;
		_trainingFile = outputFile;
		_useNoun = useNoun;
		_classifier = classifier;
		_classification = new OpinionClassification();		
	}
	
	public SubjectivityPredictionRunner(List<String> trainingType, String trainingDirectory, String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, int nGramAmount, String useNoun, boolean normalized, boolean tuneToAnnotations) {
		this(trainingType, trainingDirectory, outputFile, useWordNet, useWiktionary, useEmoticons, balanced, nGramAmount,false, false, useNoun, normalized,"-logistic",tuneToAnnotations);
	}

	public SubjectivityPredictionRunner(List<String> trainingType, String trainingDirectory, String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, int nGramAmount, boolean overwrite, boolean socialMedia, String useNoun, boolean normalized, boolean tuneToAnnotations) {
		this(trainingType, trainingDirectory, outputFile, useWordNet, useWiktionary, useEmoticons, balanced, nGramAmount,overwrite, socialMedia, useNoun, normalized,"-logistic",tuneToAnnotations);
	}
	
	public SubjectivityPredictionRunner(List<String> trainingType, String trainingDirectory, String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, int nGramAmount, boolean overwrite, boolean socialMedia, String useNoun, boolean normalized, String classifierName, boolean tuneToAnnotations) {
		
		this(outputFile, useWordNet, useWiktionary, useEmoticons, nGramAmount, overwrite, socialMedia, useNoun, classifierName);
		
		List<String> files = trainingType;
		List<String> processed = new ArrayList<String>();
		List<String> text = new ArrayList<String>();
		List<String> annotation = new ArrayList<String>();	
		
		for (int index = 0; index < files.size(); index++) {
			System.out.println("[SubjectivityPredictionRunner " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()) + "] Processing " + files.get(index));
			text.add(trainingDirectory + "/" + files.get(index) + "_text.txt");
			annotation.add(trainingDirectory + "/" + files.get(index) + "_annotation.txt");
			processed.add(OpinionRunner.preprocess(trainingDirectory + "/" + files.get(index) + "_text.txt", false, false));
		}

		_processor = new SentimentDataProcessor(
				_classification,
				new AnnotatedDataReader(false,_classification, processed,text,annotation
						,_useWordNet,_useWiktionary,_useEmoticons,balanced,_useNoun),
						outputFile,nGramAmount,overwrite,_socialMedia, _useWiktionary, normalized,tuneToAnnotations);
		train();
	}
	
	public SubjectivityPredictionRunner(AnnotatedDataReader reader, String outputFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, 
			int nGramAmount, boolean overwrite, boolean socialMedia, String useNoun, boolean normalized, boolean tuneToAnnotations) {
		
		this(outputFile, useWordNet, useWiktionary, useEmoticons, nGramAmount, overwrite, socialMedia, useNoun,"-logistic");
		
		_processor = new SentimentDataProcessor(_classification,reader,outputFile,nGramAmount,overwrite,_socialMedia, _useWiktionary,normalized,tuneToAnnotations);
		train();
	}
	
	private void train() {
		_processor.getTrainSet();
	}
}
