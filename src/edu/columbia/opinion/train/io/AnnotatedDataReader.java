package edu.columbia.opinion.train.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import edu.columbia.opinion.process.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.io.*;

public class AnnotatedDataReader extends AbstractDataReader {

	private SentenceBuilder _builder;
	private Classification _classification;
	private boolean _balanced; // if balanced
	private boolean _test;
	
	private List<String> _chunkFile;
	private List<String> _annotationFile;
	private List<String> _originalFile;

	public AnnotatedDataReader(Classification classification, String chunkFile, String originalFile, String annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun) {
		this(false, classification,Arrays.asList(chunkFile),Arrays.asList(originalFile),Arrays.asList(annotationFile), useWordNet, useWiktionary, useEmoticons, balanced, useNoun);
	}

	public AnnotatedDataReader(boolean test, Classification classification, String chunkFile, String originalFile, String annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun) {
		this(test, classification,Arrays.asList(chunkFile),Arrays.asList(originalFile),Arrays.asList(annotationFile), useWordNet, useWiktionary, useEmoticons, balanced, useNoun);
	}

	
	public AnnotatedDataReader(boolean test, Classification classification, List<String> chunkFile, List<String> originalFile, List<String> annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun) {
		_chunkFile = chunkFile;
		_originalFile = originalFile;
		_annotationFile = annotationFile;
		_classification = classification;
		_test = test;
		_builder = new SentenceBuilder(_classification.getType(), useWordNet, useWiktionary, useEmoticons, useNoun);
		_balanced = balanced;
	}
	
	public AnnotatedDataReader(Classification classification, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun) {
		this(classification,"","","", useWordNet, useWiktionary, useEmoticons, balanced, useNoun);
	}
	
	protected List<Sentence> makeSentences() {
		List<String> chunkLines = new ArrayList<String>();
        List<String> annotationLines = new ArrayList<String>();
        List<String> sentenceLines = new ArrayList<String>();
       
		for (int index = 0; index < _chunkFile.size(); index++) {
			
			if (!_balanced || _test) {
				chunkLines.addAll(Utils.readLines(_chunkFile.get(index)));
		        annotationLines.addAll(Utils.readLines(_annotationFile.get(index)));
		        sentenceLines.addAll(Utils.readLines(_originalFile.get(index)));
			}
			//balanced dataset per file
			else {
				int o_amount = 0;
			    int s_amount = 0;
			        
		        List<String> c = Utils.readLines(_chunkFile.get(index));
		        List<String> a = Utils.readLines(_annotationFile.get(index));
		        List<String> o = Utils.readLines(_originalFile.get(index));
		        
		        int i = 0;
		        
		        while (i < a.size()) {
		        	if (a.get(i).matches(".*(o|s)$")) a.set(i, new StringBuffer(a.get(i)).reverse().toString());
		        	String type = a.get(i).substring(0,1);
		        	i++;
		        	if((type.equals("o") || type.equals("objective")) && o_amount > s_amount) continue;
		        	else if (type.equals("o") || type.equals("objective")) o_amount++;
		        	else if((type.equals("s") || type.equals("subjective")) && s_amount > o_amount) continue;
		        	else s_amount++;
		        	
		        	chunkLines.add(c.get(i-1));
		        	annotationLines.add(a.get(i-1));
		        	sentenceLines.add(o.get(i-1));
		        }
			}
		}
        
        return makeSentences(chunkLines, sentenceLines, annotationLines, true);
	}
	
	public List<Sentence> makeSentences(List<String> chunkLines, List<String> sentenceLines, List<String> annotationLines) {
		return  makeSentences(chunkLines, sentenceLines, annotationLines,false);
	}
	
	public List<Sentence> makeSentences(List<String> chunkLines, List<String> sentenceLines, List<String> annotationLines, boolean balanced) {
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		
		int subjective = 0;
		int objective = 0;
		
        for (int i=0; i < chunkLines.size(); i++) {
        	String chunkLine = chunkLines.get(i);
            String annotationLine = annotationLines.get(i);
            //String sentenceLine = sentenceLines.get(i);
            String[] annParts = annotationLine.split(",");
            
			if (annotationLine.matches(".*(o|s)$")) {
				String temp = annParts[2];
				annParts[2] = annParts[1];
				annParts[1] = annParts[0];
				annParts[0] = temp;
			}
            String pol = annParts[0].startsWith("o") ? "objective" : "subjective";
            
            if (pol.equals("objective")) objective++;
            else subjective++;
                    
            int [] indices = updateIndices(sentenceLines.get(i), chunkLine,Integer.parseInt(annParts[1]),Integer.parseInt(annParts[2]));
            
            if (indices == null) continue;
            
            //if (indices[0] != Integer.parseInt(annParts[1]) || indices[1] != Integer.parseInt(annParts[2])) System.err.print("DIFF: ");
            //System.err.println("[" + annParts[1] + "," + annParts[2] + "] : " + Arrays.toString(indices));
            Sentence sentence = _builder.buildSentence(chunkLine, indices[0], indices[1], pol);
            sentences.add(sentence);
            if (edu.columbia.opinion.module.OpinionRunner.DEBUG) System.out.println(sentence.toString());
            if (chunkLines.size() > 20 && (i % (chunkLines.size() / 20) == 0)) System.out.println("[AnnotatedDataReader.makeSentences] (" + new java.util.Date() + ") Processed " + i + "/" + chunkLines.size() + " sentences.");
        }
		
        System.out.println("[AnnotatedDataReader.makeSentences] (" + new java.util.Date() + ") Completed making " + sentences.size() + " sentences. " + subjective + "/s " + objective + "/o");
        computeStatistics(sentences);
        _sentences = sentences;
		return sentences;
	}
	
	public AnnotatedDataReader subset(int start, int end, boolean exclude) {
		AnnotatedDataReader reader = new AnnotatedDataReader(_test, _classification, _chunkFile, _originalFile, _annotationFile, 
				_useWordNet, _useWiktionary, _useEmoticons, _balanced, _useNoun);

		List<Sentence> subList = new ArrayList<Sentence> ();
		
		if (exclude) {
			for (int index = 0; index < start; index++) {
				subList.add(_sentences.get(index));
			}
			for (int index = end; index < _sentences.size(); index++) {
				subList.add(_sentences.get(index));
			}
		}
		else {
			for (int index = start; index < end; index++) {
				subList.add(_sentences.get(index));
			}
		}
			
		reader.setSentences(subList);
		return reader;
	}
}
