package edu.columbia.opinion.train.io ;

import java.util.*;

import edu.columbia.opinion.process.*;
import edu.columbia.opinion.classifications.*;
import edu.columbia.opinion.io.*;

public class AnnotatedPolarityDataReader extends AbstractDataReader {

	private SentenceBuilder _builder;
	
	private Classification _classification;
	private boolean _balanced; // if balanced
	
	private List<String> _chunkFile;
	private List<String> _annotationFile;
	private List<String> _originalFile;
	private boolean _neutral;
	private boolean _test;
	
	public static void main(String[] args) throws Exception {
	}
	
	public AnnotatedPolarityDataReader(Classification classification, String chunkFile, String originalFile, String annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun, boolean neutral) {
		this(false, classification,Arrays.asList(chunkFile),Arrays.asList(originalFile),Arrays.asList(annotationFile), useWordNet, useWiktionary, useEmoticons, balanced, useNoun, neutral);
	}
	
	public AnnotatedPolarityDataReader(boolean test, Classification classification, String chunkFile, String originalFile, String annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun, boolean neutral) {
		this(test, classification,Arrays.asList(chunkFile),Arrays.asList(originalFile),Arrays.asList(annotationFile), useWordNet, useWiktionary, useEmoticons, balanced, useNoun, neutral);
	}
	
	public AnnotatedPolarityDataReader(boolean test, Classification classification, List<String> chunkFile, List<String> originalFile, List<String> annotationFile, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, boolean balanced, String useNoun, boolean neutral) {
		_chunkFile = chunkFile;
		_originalFile = originalFile;
		_annotationFile = annotationFile;
		_classification = classification;
		_builder = new SentenceBuilder(_classification.getType(), useWordNet, useWiktionary, useEmoticons, useNoun);
		_balanced = balanced;
		_neutral = neutral;
		_test = test;
	}
	
	protected List<Sentence> makeSentences() {
		List<String> chunkLines = new ArrayList<String>();
        List<String> annotationLines = new ArrayList<String>();
        List<String> sentenceLines = new ArrayList<String>();
       
		for (int index = 0; index < _chunkFile.size(); index++) {
			
			if (!_balanced || _test) {
				chunkLines.addAll(Utils.readLines(_chunkFile.get(index)));
		        annotationLines.addAll(Utils.readLines(_annotationFile.get(index)));
		        sentenceLines.addAll(Utils.readLines(_originalFile.get(index)));
			}
			//balanced dataset per file
			else {		
		        List<String> a = Utils.readLines(_annotationFile.get(index));
		        
		        chunkLines.addAll(addSentences(Utils.readLines(_chunkFile.get(index)),a));
		        annotationLines.addAll(addSentences(a,a));
		        sentenceLines.addAll(addSentences(Utils.readLines(_originalFile.get(index)),a));	
			}
		}
        
        return makeSentences(chunkLines, sentenceLines, annotationLines, true);
	}
	
	protected List<String> addSentences(List<String> sentences, List<String> annotations) {
        
        Hashtable<String, List<String>> all = new Hashtable<String, List<String>>();
        all.put("positive", new ArrayList<String>()); 
        all.put("negative", new ArrayList<String>());
        all.put("neutral", new ArrayList<String>());
        
        int i = 0;
        
        while (i < annotations.size()) {
        	String type = annotations.get(i).split(",")[0];
        	List<String> x = all.get(type);
        	x.add(sentences.get(i));
        	all.put(type, x);
        	i++;
        }
        	
        int p_size = all.get("positive").size();
        int n_size = all.get("negative").size();
        int o_size = all.get("neutral").size();
        
        int size = p_size <= n_size ? p_size : n_size;;
        
        if (_test || _neutral) size = size <= o_size ? size : o_size;
        	
        List<String> newSentences = new ArrayList<String>();
        newSentences.addAll(all.get("positive").subList(0, size));
        newSentences.addAll(all.get("negative").subList(0, size));
        if (_test || _neutral) newSentences.addAll(all.get("neutral").subList(0, size));
        
        return newSentences;
	}
	
	protected List<Sentence> makeSentences(List<String> chunkLines, List<String> sentenceLines, List<String> annotationLines) {
		return makeSentences(chunkLines, sentenceLines, annotationLines,false);
	}
	
	protected List<Sentence> makeSentences(List<String> chunkLines, List<String> sentenceLines, List<String> annotationLines, boolean balanced) {
		List<Sentence> sentences = new ArrayList<Sentence>();
		
		int positive = 0;
		int negative = 0;
		int neutral = 0;
		
        for (int i=0; i < chunkLines.size(); i++) {
        	String chunkLine = chunkLines.get(i);
            String annotationLine = annotationLines.get(i);
            //String sentenceLine = sentenceLines.get(i);
            String[] annParts = annotationLine.split(",");
            String pol = annParts[0];
            
            if (!_test && !_neutral && pol.equals("neutral")) continue;
            
            if (pol.equals("positive")) positive++;
            else if (pol.equals("negative")) negative++;
            else if (pol.equals("neutral")) neutral++;
            else pol = null;
                    
            int [] indices = updateIndices(sentenceLines.get(i), chunkLine,Integer.parseInt(annParts[1]),Integer.parseInt(annParts[2]));
            
            if (indices == null) continue;
            
            Sentence sentence = _builder.buildSentence(chunkLine, indices[0], indices[1], pol);	
            sentences.add(sentence);
            if (edu.columbia.opinion.module.OpinionRunner.DEBUG) System.out.println(sentence.toString());
            if (chunkLines.size() > 20 && (i % (chunkLines.size() / 20) == 0)) System.out.println("[AnnotatedPolarityDataReader.makeSentences] (" + new java.util.Date() + ") Processed " + i + "/" + chunkLines.size() + " sentences.");
        }
		
        System.out.println("[AnnotatedPolarityDataReader.makeSentences] (" + new java.util.Date() + ") Completed making " + sentences.size() + " sentences. " + positive + "/positive " + negative + "/negative " + (_neutral ? neutral + "/neutral" : ""));
        computeStatistics(sentences);
        return sentences;
	}
}
