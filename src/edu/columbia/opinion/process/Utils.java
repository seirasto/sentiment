package edu.columbia.opinion.process;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import weka.core.Attribute;
import weka.core.FastVector;

/**
 * @author Or
 *
 */
public class Utils {

	public static List<String> readLines(String filename) {
		BufferedReader reader = new BufferedReader(getFileReader(filename));
		List<String> lines = new ArrayList<String>();
		try {
			while (reader.ready()) {
				lines.add(reader.readLine());
			}
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException("Cannot read from file " + filename, e);
		}
		return lines;
	}

	private static FileReader getFileReader(String filename) {
		try {
			return new FileReader(filename);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found: " + filename, e);
		}		
	}

    /**
     * Get cumulative affect scores for a subsection of a sentence between
     * given start and end words
     */
    public static AffectFeatures getInnerAffectScores(List<PolarWord> tagged, int start, int finish) {

        double pleasantnessSum = 0;
        double activationSum = 0;
        double imagerySum = 0;
        //double aeSum = 0;
        //double normSum = 0;

        int count = 0;

        try {
        
	        ListIterator<PolarWord> iter = tagged.listIterator(start);
	        while(iter.hasNext() && iter.nextIndex() <= finish) {
	            PolarWord token = iter.next();
	
	            pleasantnessSum += token.getAffectScores().getPleasantness();
	            activationSum += token.getAffectScores().getActivation();
	            imagerySum += token.getAffectScores().getImagery();
	            //aeSum += token.getAffectScores().getAEscore();
	            //normSum += token.getAffectScores().getNorm();
	
	            count++;
	        }
	
	        if(count == 0) {
	        	return new AffectFeatures(0,0,0,0);
	            /*pleasantnessSum = 0;
	            activationSum = 0;
	            imagerySum = 0;
	            count = 1;*/
	        }
        } catch (Exception e) {
        	System.err.println(tagged);
        	e.printStackTrace();
        }	
	     
        double pleasantnessAvg = pleasantnessSum / count;
        double activationAvg = activationSum / count;
        double imageryAvg = imagerySum / count;
        //double normAvg = normSum / count;
        //double aeAvg = aeSum / count;
	
        double norm = 0;
        if (imagerySum != 0) {
        	norm = Math.abs(Math.sqrt(sq(pleasantnessSum) + sq(activationSum)) / imagerySum);
        }
 
        return new AffectFeatures(pleasantnessAvg, activationAvg, imageryAvg, norm);
    }

    public static double sq(double d) {
		return d*d;
	}

    public static Attribute nominalAtt(String attName, String[] nominals) {
        FastVector nominalsVector = new FastVector();
        for (String nominal : nominals) nominalsVector.addElement(nominal);
        return new Attribute(attName, nominalsVector);
	}

	public static Attribute numericAtt(String attName) {
		return new Attribute(attName);
	}

	public static Attribute stringAtt(String attName) {
		return new Attribute(attName, (FastVector)null);
	}
}
