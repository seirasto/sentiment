package edu.columbia.opinion.process;

import java.util.List;
import java.util.Properties;

import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import processing.GeneralUtils;
import social_media.Features;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.*;

public abstract class DataProcessor {

	protected static final String TEXT_ATT = "text";
	protected static final String POLAR_GRAM_TEXT_ATT = "polargramText";
	protected static final String TAG_TEXT_ATT = "tagText";
	protected static final String PLEASANTNESS_ATT = "mean_ee";
	protected static final String ACTIVITY_ATT = "mean_aa";
	protected static final String IMAGERY_ATT = "mean_ii";
	protected static final String SPACE_SCORE_ATT = "spaceScore";
    protected static final String N_WORDS = "wordCount";
    protected static final String AVG_WORD_LENGTH = "SM_avgWordLength";
    protected static final String ALL_CAP_WORDS = "SM_allCapitalWords"; 
    protected static final String CAP_WORDS = "SM_capitalWords";
    protected static final String SLANG = "SM_slang";
    protected static final String EMOTICONS = "SM_emoticons";
    protected static final String ACRONYMS = "SM_acronyms";
    protected static final String PUNCTUATION = "SM_punctuation";
	protected static final String PUNCTUATION_R = "SM_repeated_punctuation";
	protected static final String PUNCTUATION_NUM = "SM_punctuation_count";
	protected static final String EXCLAMATION = "SM_!";
	protected static final String EXCLAMATION_R = "SM_!!!";
	protected static final String QUESTION = "SM_?";
	protected static final String QUESTION_R = "SM_???";
	protected static final String ELLIPSES = "SM_...";
    protected static final String LINKS_IMAGES = "SM_linksImages";
    protected static final String WORD_LENGTHENING = "SM_word_lengthening";
    protected static final String SM_TEXT = "socialMediaText";
    protected static final String POSITION = "SM_target_position";
    protected static final String SENTIWORDNET = "sentiwordnetScore";
    protected static final String SENTIWORDNETCOUNT = "sentiwordnetCount";
    protected static final String SENTIWORDNETMAX = "sentiwordnetScoreMax";
	protected Classification _classification;

	protected static final String MIN_PLEASANTNESS_ATT = "min_ee";
	protected static final String MAX_PLEASANTNESS_ATT = "max_ee";
    
	//private static final String [] SM_TEXT_FEATURES = new String [] {"lol",":D","@","<3",":\\)",":-\\)","u",":-\\(",":\\(","\\*","'",":","-"};
	protected static final String [] SM_TEXT_FEATURES = new String [] {":D","<3",";\\)",":\\(",":\\)",";-\\)","\\^_\\^","!\\?","-",",","\\.",":","@","#","\\(","\\)","\"","&","~","`","'","\\+","\\$","\\*","idk","imo","u","lol","k"};
	
    protected String _outputFile;
    protected int _nGramSize = 100; // default
    protected boolean _socialMedia = true;
    protected boolean _normalized = true;
    protected boolean _tuneToAnnotations = true;
    
    protected Features _socialMediaGenerator;
    protected SentiWordNet _sentiWordNet = null;
    
    
    public DataProcessor(Classification classification, boolean socialMedia, boolean sentiWordNet, int nGramSize, boolean tuneToAnnotations) {
		_classification = classification;
		_nGramSize = nGramSize;
		_socialMedia = socialMedia;
		_tuneToAnnotations = tuneToAnnotations;
		
        String emoticonDirectory = null; 
    	String standardDictionary = null;
    	try {
    		Properties prop = GeneralUtils.getProperties("config/config.properties");
			emoticonDirectory = prop.getProperty("emoticons_directory");
			standardDictionary = prop.getProperty("dictionary");
			if (sentiWordNet)
				_sentiWordNet = new SentiWordNet(prop.getProperty("sentiwordnet"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	    if (_socialMedia) {	
	    	_socialMediaGenerator = new Features(standardDictionary,emoticonDirectory + "emoticon_sub.txt", 
	    			emoticonDirectory + "acronyms.txt");
		}
		
	}
	
	protected abstract Instances getBaseInstances(String name, ArrayList<Attribute> attVector, 
			AbstractDataReader reader, List<Sentence> sentenceList);
	
	protected abstract Instance sentenceToInstance(Sentence sentence, Instances instances);
	
	protected int nGramCount(String text, String attribute) {
		
		/*if ((text.indexOf("don") >= 0 && attribute.indexOf("don") >= 0) || 
			//(text.indexOf(":)") >= 0 && attribute.indexOf(":)") >= 0) || 
			(text.indexOf("good") >= 0 && attribute.indexOf("good") >= 0)) {
			System.out.println(text);
		}*/
		
		  text = text.toLowerCase();
		  attribute = attribute.toLowerCase();
		
		 int count = 0;

		 Pattern p = Pattern.compile("(^| )" + Pattern.quote(attribute) + "( |$)");
		 Matcher m = p.matcher(text);
		 
		 while (m.find()) count++;
		 
		 /*p = Pattern.compile("^" + Pattern.quote(attribute) + " ");
		 m = p.matcher(text);
		 
		 while (m.find()) count++;

		 p = Pattern.compile(" " + Pattern.quote(attribute) + "$");
		 m = p.matcher(text);
		 
		 while (m.find()) count++;
		 
		 p = Pattern.compile("^" + Pattern.quote(attribute) + "$");
		 m = p.matcher(text);
		 
		 while (m.find()) count++;*/

		 
		 return count;
	}
	
	protected StringToWordVector getCounterFilter(Instances instances, int attIndex) throws Exception {
		StringToWordVector counterFilter = new StringToWordVector();
        counterFilter.setInputFormat(instances);
        counterFilter.setAttributeIndicesArray(new int[] {attIndex});
        counterFilter.setOutputWordCounts(true);
        return counterFilter;
	}

	protected Instances reorder(Instances instances) throws Exception {
        Reorder reorderFilt = new Reorder();
        reorderFilt.setAttributeIndices("2-last,first");

        reorderFilt.setInputFormat(instances);
        return Filter.useFilter(instances, reorderFilt);
	}
	
	protected StringToWordVector getNgramFilter(Instances instances, int attIndex, int wordsToKeep, int ngramMinSize, int ngramMaxSize) throws Exception {
        StringToWordVector ngramFilt = new StringToWordVector();
        ngramFilt.setLowerCaseTokens(true);
        
        //System.err.println(Arrays.toString(ngramFilt.getOptions()));
        
        /*String[] options = new String[2];
        options[0] = "-delimiters";
        options[1] = "\" \\r\\n\\t\"";
        
        //System.err.println(Arrays.toString(options));
        
        ngramFilt.setOptions(options);*/
        
        NGramTokenizer tok = new NGramTokenizer();
        tok.setNGramMinSize(ngramMinSize);
        tok.setNGramMaxSize(ngramMaxSize);
        tok.setDelimiters(" \\r\\n\\t");
        ngramFilt.setTokenizer(tok);
        //System.err.println(Arrays.toString(ngramFilt.getOptions()));
        
        ngramFilt.setInputFormat(instances);
        ngramFilt.setAttributeIndicesArray(new int[] {attIndex});
        ngramFilt.setWordsToKeep(wordsToKeep);
     	return ngramFilt;
	}

	protected StringToWordVector getNgramFilter(Instances instances, int attIndex, int ngramMinSize, int ngramMaxSize) throws Exception {
        StringToWordVector ngramFilt = new StringToWordVector();
        ngramFilt.setLowerCaseTokens(true);
        
        String[] options = new String[2];
        options[0] = "-delimiters";
        options[1] = "\" \\r\\n\\t\"";
        
        ngramFilt.setOptions(options);
        
        NGramTokenizer tok = new NGramTokenizer();
        tok.setNGramMinSize(ngramMinSize);
        tok.setNGramMaxSize(ngramMaxSize);
        tok.setDelimiters(" \\r\\n\\t");
        ngramFilt.setTokenizer(tok);
        
        ngramFilt.setInputFormat(instances);
        ngramFilt.setAttributeIndicesArray(new int[] {attIndex});
        //ngramFilt.setWordsToKeep(wordsToKeep);
     	return ngramFilt;
	}
	
	protected Instances adjustAttributeNames(String name, Instances instances, int start, int count) {
		for (int index = start; index < count; index++) {
			instances.renameAttribute(index, name + "_" + instances.attribute(index).name());
		}
		return instances;
	}

	protected abstract ArrayList<Attribute> getAttributes();

	public String getClassName() {
		return _classification.getName();
	}
	
	protected String getCountAttName(int pol, int pos, int tag) {
		return "x" + pol + pos + tag;
	}

	public String getClassValue(int value) {
		return _classification.getValues()[value];
	}
	
	public Classification getClassification() {
		return _classification;
	}
	
	protected ArrayList<Attribute> getSMAttributes(ArrayList<Attribute> attVector) {
        
        /*
         * Social Media Features:
         * 
         * Sentence Length, Capital Words, Slang, Emoticons, Acronyms, Punctuation
         * 
         */
			attVector.add(Utils.numericAtt(POSITION));
		    if (_tuneToAnnotations) attVector.add(Utils.numericAtt(N_WORDS));
	        attVector.add(Utils.numericAtt(CAP_WORDS));
	        attVector.add(Utils.numericAtt(SLANG));
	        attVector.add(Utils.numericAtt(EMOTICONS));
	        attVector.add(Utils.numericAtt(ACRONYMS));
	        attVector.add(Utils.numericAtt(PUNCTUATION));
	    	attVector.add(Utils.numericAtt(PUNCTUATION_R));
	    	attVector.add(Utils.numericAtt(PUNCTUATION_NUM));
	    	attVector.add(Utils.numericAtt(EXCLAMATION));
	    	attVector.add(Utils.numericAtt(EXCLAMATION_R));
	    	attVector.add(Utils.numericAtt(QUESTION));
	    	attVector.add(Utils.numericAtt(QUESTION_R));
	    	attVector.add(Utils.numericAtt(ELLIPSES));
	    	attVector.add(Utils.numericAtt(LINKS_IMAGES));
	    	attVector.add(Utils.numericAtt(WORD_LENGTHENING));
	    	attVector.add(Utils.numericAtt(ALL_CAP_WORDS));
	    	attVector.add(Utils.numericAtt(AVG_WORD_LENGTH));
	    	
	    	for (int index = 0; index < SM_TEXT_FEATURES.length; index++) {
	    		attVector.add(Utils.numericAtt("SM_" + SM_TEXT_FEATURES[index]));
	    	}

        return attVector;
	}
	
	protected Instance addSMAttributes(Sentence sentence, Instances dataset, Instance instance) {

        	Hashtable<String,Double> features = _socialMediaGenerator.compute(sentence.getPlainStringArray(true));
        	//Hashtable<String,Double> featuresFull = _socialMediaGenerator.compute(sentence.getPlainStringArray(false));

        	String smString = _socialMediaGenerator.createPunctuationString(sentence.getPlainStringArray(true));
        			//sentence.getPlainString(true).replaceAll("[^\\p{Punct}++]"," ").replaceAll("\\s+"," ");
           	
        	double numWords = features.get(Features.NUM_WORDS);

    		if (!smString.trim().isEmpty()) {
	
		    	for (int index = 0; index < SM_TEXT_FEATURES.length; index++) {
		    		
		    		
		    		int count = 0;
		    		Pattern pattern = Pattern.compile(SM_TEXT_FEATURES[index],Pattern.CASE_INSENSITIVE);
		    		Matcher  matcher = pattern.matcher(smString);
		    		
		    		while (matcher.find())
		    		    count++;
		    		
		    	    // this way if a SM feature is more than one char, it will be only counted as the multiple version, not also as single char (eg ;))
		    	    smString = smString.replaceAll(SM_TEXT_FEATURES[index],"");
		    		
		    		instance.setValue(dataset.attribute("SM_" + SM_TEXT_FEATURES[index]),_normalized ? count/numWords : count);
		    	    //instance.setValue(dataset.attribute("SM_" + SM_TEXT_FEATURES[index]), count > 0 ? 1 : 0);
		    	}
    		}
	    	instance.setValue(dataset.attribute(POSITION), sentence.getTargetPosition());
            if (_tuneToAnnotations) instance.setValue(dataset.attribute(N_WORDS), numWords);
            instance.setValue(dataset.attribute(CAP_WORDS), _normalized ? features.get(Features.CAPITAL_WORDS)/numWords : features.get(Features.CAPITAL_WORDS));
            instance.setValue(dataset.attribute(SLANG), _normalized ? features.get(Features.SLANG)/numWords : features.get(Features.SLANG));
            instance.setValue(dataset.attribute(EMOTICONS), _normalized ? features.get(Features.EMOTICONS)/numWords : features.get(Features.EMOTICONS));
            instance.setValue(dataset.attribute(ACRONYMS), _normalized ? features.get(Features.ACRONYMS)/numWords : features.get(Features.ACRONYMS));
            instance.setValue(dataset.attribute(PUNCTUATION), _normalized ? features.get(Features.PUNCTUATION)/numWords : features.get(Features.PUNCTUATION));
            instance.setValue(dataset.attribute(PUNCTUATION_R), _normalized ? features.get(Features.PUNCTUATION_R)/numWords : features.get(Features.PUNCTUATION_R));
	    	instance.setValue(dataset.attribute(PUNCTUATION_NUM), _normalized ? features.get(Features.PUNCTUATION_NUM)/numWords : features.get(Features.PUNCTUATION_NUM));
	    	instance.setValue(dataset.attribute(EXCLAMATION), _normalized ? features.get(Features.EXCLAMATION)/numWords : features.get(Features.EXCLAMATION));
	    	instance.setValue(dataset.attribute(EXCLAMATION_R), _normalized ? features.get(Features.EXCLAMATION_R)/numWords : features.get(Features.EXCLAMATION_R));
	    	instance.setValue(dataset.attribute(QUESTION), _normalized ? features.get(Features.QUESTION)/numWords : features.get(Features.QUESTION));
	    	instance.setValue(dataset.attribute(QUESTION_R), _normalized ? features.get(Features.QUESTION_R)/numWords : features.get(Features.QUESTION_R));
	    	instance.setValue(dataset.attribute(ELLIPSES), _normalized ? features.get(Features.ELLIPSES)/numWords : features.get(Features.ELLIPSES));
	    	instance.setValue(dataset.attribute(LINKS_IMAGES),_normalized ? features.get(Features.LINKS)/numWords : features.get(Features.LINKS));
	    	instance.setValue(dataset.attribute(WORD_LENGTHENING),_normalized ? features.get(Features.WORD_LENGTHENING)/numWords : features.get(Features.WORD_LENGTHENING));
	    	instance.setValue(dataset.attribute(ALL_CAP_WORDS),_normalized ? features.get(Features.ALL_CAPS_WORDS)/numWords : features.get(Features.ALL_CAPS_WORDS));
        	instance.setValue(dataset.attribute(AVG_WORD_LENGTH), features.get(Features.AVG_WORD_LENGTH));
        	
	    	// binary
            /*instance.setValue(dataset.attribute(CAP_WORDS), features.get(Features.CAPITAL_WORDS) > 0 ? 1 : 0);
            instance.setValue(dataset.attribute(SLANG), features.get(Features.SLANG) > 0 ? 1 : 0);
            instance.setValue(dataset.attribute(EMOTICONS), features.get(Features.EMOTICONS)  > 0 ? 1 : 0);
            instance.setValue(dataset.attribute(ACRONYMS), features.get(Features.ACRONYMS) > 0 ? 1 : 0);
            instance.setValue(dataset.attribute(PUNCTUATION),features.get(Features.PUNCTUATION) > 0 ? 1 : 0);
            instance.setValue(dataset.attribute(PUNCTUATION_R), features.get(Features.PUNCTUATION_R) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(PUNCTUATION_NUM), features.get(Features.PUNCTUATION_NUM) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(EXCLAMATION), features.get(Features.EXCLAMATION) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(EXCLAMATION_R), features.get(Features.EXCLAMATION_R) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(QUESTION), features.get(Features.QUESTION) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(QUESTION_R), features.get(Features.QUESTION_R) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(ELLIPSES), features.get(Features.ELLIPSES) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(LINKS_IMAGES), features.get(Features.LINKS) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(WORD_LENGTHENING), features.get(Features.WORD_LENGTHENING) > 0 ? 1 : 0);
	    	instance.setValue(dataset.attribute(ALL_CAP_WORDS), features.get(Features.ALL_CAPS_WORDS) > 0 ? 1 : 0);
        	instance.setValue(dataset.attribute(AVG_WORD_LENGTH), features.get(Features.AVG_WORD_LENGTH));*/
        
        return instance;
    }
	
	protected Instance sentenceToInstance(Sentence sentence, Instance instance, Instances dataset) {
		AffectFeatures scores = sentence.getAffectScores();
		
		instance.setValue(dataset.attribute(TEXT_ATT), sentence.getPlainString(true).toLowerCase());
        instance.setValue(dataset.attribute(POLAR_GRAM_TEXT_ATT), sentence.getPolarChunkString(true));
        instance.setValue(dataset.attribute(TAG_TEXT_ATT), sentence.getPosString(true));
        instance.setValue(dataset.attribute(PLEASANTNESS_ATT), scores.getPleasantness());
        instance.setValue(dataset.attribute(ACTIVITY_ATT), scores.getActivation());
        instance.setValue(dataset.attribute(IMAGERY_ATT), scores.getImagery());
        instance.setValue(dataset.attribute(SPACE_SCORE_ATT), scores.getNorm());
        //instance.setValue(dataset.attribute(SENTIWORDNETMAX), sentence.sentiWordNetMax(_sentiWordNet, true));   

        // social media features
        if (_socialMedia) {
        	instance = this.addSMAttributes(sentence, dataset, instance);
        }
        
        double[] minmaxee = sentence.getMinMaxTargetPleasantness();
        
        if (minmaxee == null) {
        	System.err.println("[OpinionDataProcessor.sentenceToInstance] Error getting min max pleasantness for s: " + sentence.getPlainString(false));
            return null;
        }

        instance.setValue(dataset.attribute(MIN_PLEASANTNESS_ATT), minmaxee[0]);
        instance.setValue(dataset.attribute(MAX_PLEASANTNESS_ATT), minmaxee[1]);
        
        return instance;
	}
	
	protected ArrayList<Attribute> getAttributes(ArrayList<Attribute> attVector) {
		
        attVector.add(Utils.nominalAtt(_classification.getName(), _classification.getValues()));
        attVector.add(Utils.stringAtt(POLAR_GRAM_TEXT_ATT));
        attVector.add(Utils.stringAtt(TEXT_ATT));
        //if (_socialMedia) attVector.add(Utils.stringAtt(SM_TEXT));
        attVector.add(Utils.stringAtt(TAG_TEXT_ATT));
        attVector.add(Utils.numericAtt(PLEASANTNESS_ATT));
        attVector.add(Utils.numericAtt(ACTIVITY_ATT));
        attVector.add(Utils.numericAtt(IMAGERY_ATT));
        attVector.add(Utils.numericAtt(SPACE_SCORE_ATT));
        attVector.add(Utils.numericAtt(MAX_PLEASANTNESS_ATT));
        attVector.add(Utils.numericAtt(MIN_PLEASANTNESS_ATT));
        if (_sentiWordNet != null) attVector.add(Utils.numericAtt(SENTIWORDNET));
    	//attVector.add(Utils.numericAtt(SENTIWORDNETMAX));
        
        /*
         * Social Media Features:
         * 
         * Sentence Length, Capital Words, Slang, Emoticons, Acronyms, Punctuation
         * 
         */
        if (_socialMedia) {
        	attVector = this.getSMAttributes(attVector);
        }
        return attVector;
	}
	
	protected ArrayList<Attribute> getAttributesPolarity() {
		
        ArrayList<Attribute> attVector = new ArrayList<Attribute>();
        attVector = getAttributes(attVector);
        
        Attribute[][][] countAtts = new Attribute[3][PolarChunk.NUM_POSITIONS][PolarChunk.NUM_TAGS];

        for (int pol = 0; pol < 3; pol++) {
            for (int pos = 0; pos < PolarChunk.NUM_POSITIONS; pos++) {
                for (int tag = 0; tag < PolarChunk.NUM_TAGS; tag++) {
                    Attribute att = Utils.numericAtt(getCountAttName(pol, pos, tag));
                    countAtts[pol][pos][tag] = att;
                    attVector.add(att);
                }
            }
        }

        return attVector;
	}
 	
	protected Instance sentenceToInstancePolarity(Sentence sentence, Instances dataset) {

        Instance instance = new DenseInstance(8 + (_socialMedia ? 17 + SM_TEXT_FEATURES.length : 0) + (_socialMedia && _tuneToAnnotations ? 1 : 0) + 2 + (_sentiWordNet != null ? 1 : 0) + (3 * PolarChunk.NUM_POSITIONS * PolarChunk.NUM_TAGS));
        
        instance = sentenceToInstance(sentence, instance, dataset);
        
        if (_sentiWordNet != null) {
        	instance.setValue(dataset.attribute(SENTIWORDNET), sentence.sentiWordNetScore(_sentiWordNet, true, false));
        }
        
        int[][][] counts = sentence.countPolarChunks(3);
        for (int pol = 0; pol < 3; pol++) {
            for (int pos = 0; pos < PolarChunk.NUM_POSITIONS; pos++) {
                for (int tag = 0; tag < PolarChunk.NUM_TAGS; tag++) {
                    instance.setValue(dataset.attribute(getCountAttName(pol, pos, tag)), counts[pol][pos][tag]);
                }
            }
        }

        return instance;
    }
	
	protected ArrayList<Attribute> getAttributesSubjectivity() {
		
        ArrayList<Attribute> attVector = new ArrayList<Attribute>();
        attVector = getAttributes(attVector);
        
        if (_sentiWordNet != null)
        	attVector.add(Utils.numericAtt(SENTIWORDNETCOUNT));
    	
        Attribute[][][] countAtts = new Attribute[2][PolarChunk.NUM_POSITIONS][PolarChunk.NUM_TAGS];

        for (int pol = 0; pol < 2; pol++) {
            for (int pos = 0; pos < PolarChunk.NUM_POSITIONS; pos++) {
                for (int tag = 0; tag < PolarChunk.NUM_TAGS; tag++) {
                    Attribute att = Utils.numericAtt(getCountAttName(pol, pos, tag));
                    countAtts[pol][pos][tag] = att;
                    attVector.add(att);
                }
            }
        }

        return attVector;
	}
 	
	protected Instance sentenceToInstanceSubjectivity(Sentence sentence, Instances dataset) {
        Instance instance = new DenseInstance(9 + (_socialMedia ? 17 + SM_TEXT_FEATURES.length : 0) + (_socialMedia && _tuneToAnnotations ? 1 : 0) + (_sentiWordNet != null ? 2 : 0) + 1 + (2 * PolarChunk.NUM_POSITIONS * PolarChunk.NUM_TAGS));
        
        instance = sentenceToInstance(sentence, instance, dataset);
        
        if (_sentiWordNet != null) {
	        instance.setValue(dataset.attribute(SENTIWORDNET), sentence.sentiWordNetScore(_sentiWordNet, true, true));
	        instance.setValue(dataset.attribute(SENTIWORDNETCOUNT), sentence.sentiWordNetCount(_sentiWordNet, true, true) + sentence.sentiWordNetCount(_sentiWordNet, true, false));   
        }
        
        int[][][] counts = sentence.countPolarChunks(2);
        for (int pol = 0; pol < 2; pol++) {
            for (int pos = 0; pos < PolarChunk.NUM_POSITIONS; pos++) {
                for (int tag = 0; tag < PolarChunk.NUM_TAGS; tag++) {
                    instance.setValue(dataset.attribute(getCountAttName(pol, pos, tag)), counts[pol][pos][tag]);
                }
            }
        }

        return instance;
    }
}
