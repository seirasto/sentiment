package edu.columbia.opinion.process;

/**
 * Word tagged with prior polarity
 * 
 * @author Jacob, modified by Or
 */
public class PolarWord {

	public static final int WORDNET = 0;
	public static final int WIKTIONARY = 1;
	public static final int FAKE = 2;
	public static final int DAL = -1;
	
    private String _word;
    private String _tag;

    private AffectFeatures _polarities;
    
    private int _type = DAL;
    private int _degree = 0;

    public PolarWord(String word, String tag, AffectFeatures polarities) {
        _word = word;
        _tag = tag;
        _polarities = polarities;
    }
    
    public PolarWord(String word, String tag, AffectFeatures polarities, int type, int degree) {
        _word = word;
        _tag = tag;
        _polarities = polarities;
        _type = type;
        _degree = degree;
    }

    public String getWord() {
        return _word;
    }

    public String getTag() {
        return _tag;
    }

    public AffectFeatures getAffectScores() {
        return _polarities;
    }


    public int getDegree() {
		return _degree;
	}
	
	public int getResourceType() {
		return _type;
	}
	
	public String getResourceTypeToString() {
		if (_type == WIKTIONARY) return "wiktionary";
		else if (_type == WORDNET) return "wordnet";
		else if (_type == FAKE) return "dummy";
		else return "DAL";
			
	}
	
	public String toString() {
		String string = _word + (_tag != null ? "/" + _tag : "");
		if (_type >=0) {
			string += " (" + (_type == WIKTIONARY ? "wiktionary" : "wordnet") + ", at " + _degree + "): ";
		}
		string += _polarities.toString();  
		return string;
	}
}
