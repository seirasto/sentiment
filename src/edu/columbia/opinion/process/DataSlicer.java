package edu.columbia.opinion.process;

import java.util.ArrayList;
import java.util.List;
import weka.core.Instances;

import edu.columbia.opinion.io.AbstractDataReader;

public class DataSlicer {

	private List<Sentence> _train = new ArrayList<Sentence>();
	private List<Sentence> _test = new ArrayList<Sentence>();
	
	public DataSlicer(AbstractDataReader reader, double testSlice) {
		double c = 0;
		
		for (Sentence sentence : reader.getSentences()) {
			c += testSlice;
			
			if (c >= 1) {
				_test.add(sentence);
				c -= 1;
			}
			else {
				_train.add(sentence);
			}
		}
	}

	public AbstractDataReader getTrainReader() {
		return new AbstractDataReader() {
			public List<Sentence> getSentences() {
				return _train;
			}
			public Instances getInstances() {
				return null;
			}
			public List<Sentence> makeSentences() {
				return _train;
			}
		};
	}

	public AbstractDataReader getTestReader() {
		return new AbstractDataReader() {
			public List<Sentence> getSentences() {
				return _test;
			}
			public Instances getInstances() {
				return null;
			}
			public List<Sentence> makeSentences() {
				return _test;
			}
		};
	}

}
