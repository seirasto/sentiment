package edu.columbia.opinion.process;

import java.util.*;

/**
 * @author Jacob, modified by Or
 */
public class Sentence {

    //private List<String> _plain = new ArrayList<String>();
    private List<PolarWord> _tagged = new ArrayList<PolarWord>();
    private List<PolarChunk> _chunked = new ArrayList<PolarChunk>();
    
    // start and end of target phrase
    int _start;
    int _end;
    
    private String _classification;
    
	public Sentence(List<String> plain, List<PolarWord> tagged, List<PolarChunk> chunked, int start, int end, String classification) {
		//_plain = plain;
		_tagged = tagged;
		_chunked = chunked;
		_classification = classification;
		_start = start;
		_end = end;
	}
	
	public int getStart() {
		return _start;
	}
	
	public int getEnd() {
		return _end;
	}
	
	public int getStartCharacter() {
		
		int index = 0;
		int start = 0;
		
		if (_start == 0) return 0;
		
		for (PolarWord word : _tagged) {
			if (index >= _start) break;
			start += word.getWord().length() + 1;
			index++;
		}
		return start - 1;
	}
	
	public int getEndCharacter() {
		
		int index = 0;
		int end = 0;
		
		for (PolarWord word : _tagged) {
			if (index > _end) break;
			end += word.getWord().length() + 1;
			index++;
		}
		return end - 1;
	}

    public String getClassification() {
    	return _classification;
    }

    /**
     * Get affect scores for target phrase
     */
    public AffectFeatures getAffectScores() {
        return Utils.getInnerAffectScores(_tagged, _start, _end);
    }

    public String getPlainString() {
    	return getPlainString(false);
    }
    
    /**
     * Get plaintext of sentence (suitable for creating ngrams)
     */
    public String getPlainString(boolean targetOnly) {
    	String str = "";
    	
    	for (PolarChunk chunk : _chunked) {
            if(targetOnly && chunk.getPosition() != PolarChunk.TARGET) continue;
    		str += chunk.getPlainString() + " ";
    	}
    	return str.trim();
    }
    
    public int getTargetPosition() {
    	if (_chunked.get(0).getPosition() == PolarChunk.TARGET) return -1;
    	if (_chunked.get(_chunked.size()-1).getPosition() == PolarChunk.TARGET) return 1;
    	return 0;
    }
    
    /**
     * Get plaintext of sentence (suitable for creating ngrams)
     */
    public String [] getPlainStringArray(boolean targetOnly) {
    	ArrayList<String> str = new ArrayList<String>();
    	
    	for (PolarChunk chunk : _chunked) {
            if(targetOnly && chunk.getPosition() != PolarChunk.TARGET) continue;
            List<PolarWord> words = chunk.getTaggedWords();
            
            for (PolarWord word : words) str.add(word.getWord());
    	}
    	String []strArray = new String[str.size()];
    	return str.toArray(strArray);
    }    

    /**
     * Get plaintext of sentence (suitable for creating ngrams)
     */
    public PolarWord [] getPolarWordArray(boolean targetOnly) {
    	ArrayList<PolarWord> str = new ArrayList<PolarWord>();
    	
    	for (PolarChunk chunk : _chunked) {
            if(targetOnly && chunk.getPosition() != PolarChunk.TARGET) continue;
            List<PolarWord> words = chunk.getTaggedWords();
            
            for (PolarWord word : words) str.add(word);
    	}
    	PolarWord []strArray = new PolarWord[str.size()];
    	return str.toArray(strArray);
    }    
    
    /**
     * Get a tagged chunk representation of this sentence
     */
    public String getPolarChunkString(boolean targetOnly) {
    	String str = "";
    	for (PolarChunk chunk : _chunked) {
            if(targetOnly && chunk.getPosition() != PolarChunk.TARGET) continue;
            
    		str += chunk.getTag() + "_" + chunk.getPolarity() + "_" + chunk.getPosition() + " ";
    	}
    	return str.trim();
    }

    public double[] getMinMaxTargetPleasantness() {
    	if (_chunked.isEmpty()) return null;
    	
        double min = _chunked.get(0).getMeanPleasantness();
        double max = _chunked.get(0).getMeanPleasantness();
        
        for (PolarChunk chunk : _chunked) {
            if (chunk.getPosition() != PolarChunk.TARGET) continue;
            
            double mp = chunk.getMeanPleasantness();
            if (mp < min) min = mp;
            if (mp > max) max = mp;
        }
        
        return new double[] {min, max};
    }

    public double[] getMinMaxTargetImagery() {
    	if (_chunked.isEmpty()) return null;
    	
        double min = _chunked.get(0).getMeanImagery();
        double max = _chunked.get(0).getMeanImagery();
        
        for (PolarChunk chunk : _chunked) {
            if (chunk.getPosition() != PolarChunk.TARGET) continue;
            
            double mp = chunk.getMeanImagery();
            if (mp < min) min = mp;
            if (mp > max) max = mp;
        }
        
        return new double[] {min, max};
    }

	public int[][][] countPolarChunks(int numPolarities) {
        int[][][] counts = new int[numPolarities][PolarChunk.NUM_POSITIONS][PolarChunk.NUM_TAGS];
        for(PolarChunk chk : _chunked) {
            counts[chk.getPolarity()][chk.getPosition()][chk.getTag()]++;
        }
        return counts;
    }

    public String getPosString(boolean targetOnly) {
        StringBuilder out = new StringBuilder();
        for(PolarChunk chk : _chunked) {
            if(targetOnly && chk.getPosition() != PolarChunk.TARGET) continue;
            
            for(PolarWord p : chk.getTaggedWords()) {
                out.append(p.getTag());
                out.append(" ");
            }
        }
        return out.toString().trim();
    }
    
	public double sentiWordNetScore(SentiWordNet sentiWordNet, boolean targetOnly, boolean subjective) {
		
		PolarWord [] words = this.getPolarWordArray(targetOnly);
		
		double score = 0;
		
		for (PolarWord w : words) {
			String word = w.getWord();
			String pos = convertToSentiWordNetPos(w.getTag());
			
			if (pos == null) continue;
			
			if (sentiWordNet.contains(word.toLowerCase(), pos))
				if (subjective) score += Math.abs(sentiWordNet.extract(word.toLowerCase(), pos));
				else score += sentiWordNet.extract(word.toLowerCase(), pos);
		}
		return score;
	}
	
	public double sentiWordNetMax(SentiWordNet sentiWordNet, boolean targetOnly) {
		double pos = sentiWordNetMax(sentiWordNet, targetOnly, true);
		double neg = sentiWordNetMax(sentiWordNet, targetOnly, true);
		
		if (pos > neg) return pos;
		return neg;
	}
	
	public double sentiWordNetCount(SentiWordNet sentiWordNet, boolean targetOnly, boolean positive) {
		PolarWord [] words = this.getPolarWordArray(targetOnly);
		
		double score = 0;
		
		for (PolarWord w : words) {
			String word = w.getWord();
			String pos = convertToSentiWordNetPos(w.getTag());
			
			if (pos == null) continue;
			
			if (sentiWordNet.contains(word.toLowerCase(), pos)) {
				if (positive && sentiWordNet.extract(word.toLowerCase(), pos) > 0) score++;
				else if (!positive && sentiWordNet.extract(word.toLowerCase(), pos) < 0) score++;
			}
		}
		return score;
	}
	
	public double sentiWordNetMax(SentiWordNet sentiWordNet, boolean targetOnly, boolean positive) {
		PolarWord [] words = this.getPolarWordArray(targetOnly);
		
		double score = 0;
		
		for (PolarWord w : words) {
			String word = w.getWord();
			String pos = convertToSentiWordNetPos(w.getTag());
			
			if (pos == null) continue;
			
			if (sentiWordNet.contains(word.toLowerCase(), pos)) {
				double s = sentiWordNet.extract(word.toLowerCase(), pos);
				if (positive && s > score) score = s;
				else if (!positive && s < score) score = s;
			}
		}
		return score;
	}
	
	public double polarSentiWordNetScore(SentiWordNet sentiWordNet, boolean targetOnly) {
		
		PolarWord [] words = this.getPolarWordArray(targetOnly);
		
		double score = 0;
		
		for (PolarWord w : words) {
			String word = w.getWord();
			String pos = convertToSentiWordNetPos(w.getTag());
			
			if (pos == null) continue;
			
			if (sentiWordNet.contains(word.toLowerCase(), pos))
				score += sentiWordNet.extract(word.toLowerCase(), pos);
		}
		
		if (score >= 3) return 1;
		if (score <= -3) return -1;
		return 0;
	}
	
	public static String convertToSentiWordNetPos(String pos) {
    	if (pos == null) return null;
    	if(pos.equals("JJ")) return "a";
        if(pos.matches("NN[A-Z]?")) return "n";
        if(pos.equals("RB")) return "r";
        if(pos.matches("VB[A-Z]?")) return "v";
        
        return null; 
	}

    public String toString() {
    	String string = "Sentence: " + this.getPlainString(false) + "\n";
    	//string += "Tagged: " + this.getPlainString() + "\n";
    	//string += "Chunked: " + this.getPolarChunkString() + "\n";

    	for (PolarChunk c : _chunked) {
    		string += c.toString(); 
    	}
    	
    	return string += "\n";
    }
}
