package edu.columbia.opinion.process;

import java.util.List;
import java.util.ArrayList;
import edu.columbia.opinion.io.*;
import edu.columbia.opinion.classifications.*;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public abstract class TestDataProcessor extends DataProcessor {
  
    protected List<Sentence> _trainSentences;
    protected List<Sentence> _testSentences;
    
    public TestDataProcessor(Classification classification, boolean socialMedia, boolean sentiWordNet, int _nGramSize, boolean tuneToAnnotations) {
    	super(classification, socialMedia, sentiWordNet, _nGramSize, tuneToAnnotations);
	}
	
	public Instances getTestSet(AbstractDataReader testReader, Instances trainAttributes) {
		Instances test = makeTestDataset(testReader, trainAttributes);
		return test;
	}
	
	protected Instances makeTestDataset(AbstractDataReader testReader, Instances trainAttributes) {
		try {
			Instances test = getBaseInstances("Test set", getAttributes(), testReader, _testSentences = new ArrayList<Sentence>());
			test.setClassIndex(0);
			test = finalizeTestSet(test, trainAttributes);
	    	test.setClass(test.attribute(getClassName()));
	    	return test;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}
	
	protected Instances getBaseInstances(String name, ArrayList<Attribute> attVector, 
			AbstractDataReader reader, List<Sentence> sentenceList) {
        Instances instances = new Instances(name, attVector, 0);
                
        for (Sentence sentence : reader.getSentences()) {
            Instance instance = sentenceToInstance(sentence, instances);
            if(instance == null)
            	{
            	  continue;
            	}
            
            if (sentence.getClassification() != null && 
            		(getClassName().equals(PolarityClassification.POLARITY_NAME) && 
            				!sentence.getClassification().equals("neutral") || getClassName().equals(PolarityClassification.NEUTRAL_NAME) || getClassName().equals(OpinionClassification.NAME)) ) {
            	instance.setValue(instances.attribute(getClassName()), sentence.getClassification());
            }
            
            instances.add(instance);
            sentenceList.add(sentence);
        }
        //System.exit(0); // when computing sm stats
        return instances;
	}
	
	public Instances finalizeTestSet(Instances test, Instances attributes) throws Exception {
       
        // put class last
        test = reorder(test);
		
		for (int index = 0; index < test.numInstances(); index++) {
		
			Instance testInstance = test.instance(index);
			SparseInstance newInstance = new SparseInstance(attributes.numAttributes());
			
			for (int aIndex = 0; aIndex < attributes.numAttributes(); aIndex++) {
				// training attributes IS NOT in test set. Set to 0.
				if (test.attribute(attributes.attribute(aIndex).name()) == null) {
					// if its one of the string features compute it
					// change special characters 
					
					int value = 0;
					// PC - 0
					if (attributes.attribute(aIndex).name().startsWith("PC_")) {
						value = nGramCount(testInstance.stringValue(0),attributes.attribute(aIndex).name().substring(3));
						if (value > 0) value = 1; // use binary for polar chunks
					}
					// NG - 1
					else if (attributes.attribute(aIndex).name().startsWith("NG_")) {
						value = nGramCount(testInstance.stringValue(1),attributes.attribute(aIndex).name().substring(3));
						if (value > 0) value = 1; // use binary for ngrams
					}
					// POS - 2
					else if (attributes.attribute(aIndex).name().startsWith("POS_")) {
						value = nGramCount(testInstance.stringValue(2),attributes.attribute(aIndex).name().substring(4));
					}
					newInstance.setValue(attributes.attribute(aIndex), value);
				}
				// training attributes IS in test set. Copy value 
				else {
					int testPosition = test.attribute(attributes.attribute(aIndex).name()).index();
					newInstance.setValue(attributes.attribute(aIndex), testInstance.value(testPosition));
				}
			}
			attributes.add(newInstance);
		}
			
		return attributes;
	}

	public List<Sentence> getTestSentences() {
		return _testSentences;
	}

	public List<Sentence> getTrainSentences() {
		return _trainSentences;
	}
}
