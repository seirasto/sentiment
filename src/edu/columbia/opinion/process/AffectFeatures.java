package edu.columbia.opinion.process;

/**
 * Represents a set of affect scores
 */
public class AffectFeatures {

    private double _pleasantness;
    private double _activation;
    private double _imagery;
    private double _norm;
    private double _AEscore;
    private double _weight;
    
    public AffectFeatures(double ee, double aa, double ii, double norm) {
    	this(ee,aa,ii,norm,1);
    }
    
    public AffectFeatures(double ee, double aa, double ii, double norm, double weight) {
        _pleasantness = ee;
        _activation = aa;
        _imagery = ii;
        _norm = norm;
        _weight = weight;
        _AEscore = Math.signum(_pleasantness) * Math.sqrt(Utils.sq(_pleasantness) + Utils.sq(_activation));
        
    }

    public void setWeight(double weight) { _weight = weight; }
    public double getPleasantness() { return _pleasantness; }
    public double getActivation() { return _activation; }
    public double getImagery() { return _imagery; }
    public double getNorm() { return _norm; }
    public double getWeight() { return _weight; }
    public double getAEscore() { return _AEscore; }
   
    public String toString() {
    	return "ee: " + _pleasantness + ", aa: " + _activation + ", ii: " + _imagery;
    }
}
