package edu.columbia.opinion.process;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.io.File;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.HashMap;

import wiktionary.*;
import processing.*;
import edu.columbia.opinion.classifications.*;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.data.PointerUtils;
import net.didion.jwnl.data.list.PointerTargetNodeList;
import net.didion.jwnl.data.list.PointerTargetNode;

import edu.stanford.nlp.process.Morphology;



public class SentenceBuilder {
	
	public static final double SUBJECTIVE_THRESHOLD = 0;
    //  Empirically determined, see Agarwal 2009
   public static final double NEUTRAL_THRESHOLD = 0.7;
    
	protected List<String> _plain;
	protected List<PolarWord> _tagged;
	protected List<PolarChunk> _chunked;
	int _classifierType;
	protected static boolean _useWordNet;
	protected static boolean _useWiktionary;
	protected static boolean _useEmoticons;
	protected static String _useNoun;
	protected static EmoticonDictionary _emoticonDictionary;
	protected static Wiktionary _wiktionary;
	protected static DAL _DAL;
	
	public static void main(String[] args) throws Exception {

		String chunkLine = //"Perhaps/RB/B-ADVP if/IN/B-SBAR reported/VBN/B-VP critically/RB/B-ADVP by/IN/B-PP a/DT/B-NP western/JJ/I-NP source/NN/I-NP but/CC/O certainly/RB/B-ADVP not/RB/O by/IN/B-PP an/DT/B-NP Israeli/JJ/I-NP source/NN/I-NP ././O";
			"RT/NNP/B-NP @/#/I-NP tash_jade/NN/I-NP :/:/O That/DT/B-NP is/VBZ/B-VP really/RB/B-ADVP sad/JJ/B-ADJP ,/,/O Charlie/NNP/B-NP RT/NNP/I-NP \\/POS/B-NP \"/EX/I-NP Until/IN/B-PP tonight/NN/B-NP I/PRP/B-NP never/RB/B-ADVP realised/VBD/B-VP how/WRB/B-ADVP fucked/VBN/B-VP up/IN/B-PP I/PRP/B-NP was\\/VBP/B-VP \"/SYM/B-UCP -/:/B-NP Charlie/NNP/I-NP Sheen/NNP/I-NP #/#/I-NP sheenroast/NN/I-NP";
			//"i/PRP/B-NP will/MD/B-VP have/VB/I-VP to/TO/I-VP stick/VB/I-VP to/TO/B-PP my/PRP$/B-NP canon/NN/I-NP film/NN/I-NP slr/NN/I-NP until/IN/B-PP in/IN/B-PP a/DT/B-NP few/JJ/I-NP years/NNS/I-NP i/PRP/B-NP can/MD/B-VP afford/VB/I-VP to/TO/I-VP upgrade/VB/I-VP again/RB/B-NP :)/CD/I-NP";
			//"The/DT/B-NP sale/NN/I-NP infuriated/VBD/B-VP Beijing/NNP/B-NP which/WDT/B-NP regards/VBZ/B-VP Taiwan/NNP/B-NP an/DT/B-NP integral/JJ/I-NP part/NN/I-NP of/IN/B-PP its/PRP$/B-NP territory/NN/I-NP awaiting/VBG/I-NP reunification/NN/I-NP ,/,/O by/IN/B-PP force/NN/B-NP if/IN/B-SBAR necessary/JJ/B-ADJP ././O";
		int phraseStart = 0;
			//0 1 5;
		int phraseEnd = 0;
			//2 4 10;
		String classification = "s";
			//"o s s";
		
		SentenceBuilder sb = new SentenceBuilder(Classification.SUBJECTIVITY,true,true,true,"NNP");
		Sentence sentence = sb.buildSentence(chunkLine, phraseStart, phraseEnd, classification);
			System.out.println(sentence.getPlainString().substring(sentence.getStartCharacter(),sentence.getEndCharacter()));
	}
	
	public SentenceBuilder(int classifierType, boolean useWordNet, boolean useWiktionary, boolean useEmoticons, String useNoun) {
		_classifierType = classifierType;
		_useWordNet = useWordNet;
		_useWiktionary = useWiktionary;
		_useEmoticons = useEmoticons;
		if (useEmoticons) _emoticonDictionary = loadDictionary();
		if (useWiktionary) _wiktionary = loadWiktionary();
		_DAL = DAL.getInstance();
		_useNoun = useNoun;
	}
	
	public SentenceBuilder(int classifierType) {
		this(classifierType,false,false,false,"");
	}
	
	public Sentence buildSentence(String chunkLine, int phraseStart, int phraseEnd, 
			String classification ) {
		_plain = new ArrayList<String>();
		_tagged = new ArrayList<PolarWord>();
		_chunked = new ArrayList<PolarChunk>();
	
		// label features
		//System.out.println("before annotate() in sentence builder");
		//System.out.println("Chunk line is: " + chunkLine);
        annotate(chunkLine);
        //System.out.println("after annotate() in sentence builder");
        markSyntacticFeatures(phraseStart, phraseEnd);
        

		return new Sentence(_plain, _tagged, _chunked, phraseStart, phraseEnd, classification);
	}
	   /**
     * Converts from chunked String representation to polar chunk object representation
     */
    private void annotate(String chunkStr) {
        NegationStateMachine nsm = new NegationStateMachine();

        // split chunk string at spaces
        String[] splitChunkStr = chunkStr.split("\\s+");

        PolarChunk activeChunk = null;
  
        for (String tok : splitChunkStr) {
        	String chunkTag = "";
        	String posTag = "";
        	String word = "";
        		
        	try {
	            chunkTag = tok.substring(tok.lastIndexOf("/")+1);
	            tok = tok.substring(0,tok.lastIndexOf("/"));
	            
	            posTag = tok.substring(tok.lastIndexOf("/")+1);
	            tok = tok.substring(0,tok.lastIndexOf("/"));
	            
	            word = tok;
        	} catch (Exception e) {
        		System.err.println("[SentenceBuilderImpl.annotate] Error splitting " + tok);
        		continue;
        	}
        	
            _plain.add(word);

            // get main chunk label (B,O,I)
            String chunkLabel = "";
            if (chunkTag.indexOf("-") >= 0) {
                chunkLabel = chunkTag.substring(0,1);
                chunkTag = chunkTag.substring(2);
            }
            else {
            	chunkLabel = chunkTag;
            	chunkTag = "";
            }
            
            // get individual word polarity and label it
            AffectFeatures affectScores = getAffectScores(word, posTag);
            if (affectScores == null) affectScores = new AffectFeatures(0,0,0,0);
                        
            affectScores = new AffectFeatures(affectScores.getPleasantness() * nsm.getSign(), affectScores.getActivation(), affectScores.getImagery(), affectScores.getNorm());
            nsm.next(word, posTag);

            PolarWord taggedWord = new PolarWord(word, posTag, affectScores);
            _tagged.add(taggedWord);
            
            // push this into the current chunk if label is I or O
            // B starts new chunk
            if (chunkLabel.equals("B") || activeChunk == null || (chunkLabel.equals("O") && (_emoticonDictionary != null && _emoticonDictionary.hasEmoticon(word)))) {
                
            	if (activeChunk != null) {
                    _chunked.add(activeChunk);
                }
                activeChunk = new PolarChunk(chunkTag, _classifierType == Classification.POLARITY ? false : true);
                activeChunk.add(taggedWord);
            }
            else {
                activeChunk.add(taggedWord);	
            }
        }
        // add the last chunk
        if (activeChunk != null) {
            _chunked.add(activeChunk);
        }
    }

	/**
     * Determine boundaries of expanded target phrase, and mark before, inside and after
     */
    private void markSyntacticFeatures(int phraseStart, int phraseEnd) {

        int startChunk = 0;
        int endChunk = 0;

        int firstWord = 0;
        int chunkNo = 0;

        for (PolarChunk chk : _chunked) {
            if (firstWord <= phraseStart && phraseStart < firstWord + chk.length()) {
                // this is the first word in the chunk
                startChunk = chunkNo;
            }
            if (firstWord <= phraseEnd && phraseEnd < firstWord + chk.length()) {
                // this is the last word in the chunk
                endChunk = chunkNo + 1;
            }
            chunkNo++;
            firstWord += chk.length();
        }

        if(endChunk == 0) {
            endChunk = _chunked.size();
        }

        markPolarChunks(0, startChunk, PolarChunk.BEFORE);
        markPolarChunks(startChunk, endChunk, PolarChunk.TARGET);
        markPolarChunks(endChunk, _chunked.size(), PolarChunk.AFTER);

    }
    
    /**
     * Mark tags for specific subset of sentence (before, inside or after target section)
     */
    protected void markPolarChunks(int start, int finish, int position) {
        ListIterator<PolarChunk> iter = _chunked.listIterator(start);
        while(iter.hasNext() && iter.nextIndex() < finish) {
            PolarChunk chunk = iter.next();
            List<PolarWord> words = chunk.getTaggedWords();
            AffectFeatures chunkScores = Utils.getInnerAffectScores(words, 0, words.size()-1);
            
            double pleasantness = chunkScores.getPleasantness();
            double imagery = chunkScores.getImagery();
            
            if (_classifierType == Classification.POLARITY) {
            	double activation = chunkScores.getActivation();
                double spaceScore = Math.signum(pleasantness) * Math.sqrt(Utils.sq(pleasantness) + Utils.sq(activation));
            	int polarity = getPolarity(spaceScore);
            	chunk.setPolarity(polarity);
            }
            else { //if (_classifierType == Classification.Subjective) {
            	int subjectivity = isSubjective(chunkScores.getAEscore(), imagery);
            	chunk.setPolarity(subjectivity);
            }
            
            // subjective chunks are marked as targets
            chunk.setPosition(position);
            chunk.setMeanPleasantness(pleasantness);
            chunk.setMeanImagery(imagery);
        }
    }        

    private static int getPolarity(double spaceScore) {
        if (spaceScore > NEUTRAL_THRESHOLD) return PolarChunk.POSITIVE;
        if (spaceScore < -NEUTRAL_THRESHOLD) return PolarChunk.NEGATIVE;
        return PolarChunk.NEUTRAL;
	}
    
    private static int isSubjective(double score, double imagery) {    	
    	if (getPolarity(score) != PolarChunk.NEUTRAL) return PolarChunk.SUBJECTIVE;
    	return imagery < SUBJECTIVE_THRESHOLD ? PolarChunk.SUBJECTIVE : PolarChunk.OBJECTIVE;
    }
    
    public static EmoticonDictionary loadDictionary() {
    	try {
    		
    		String directory = null;
    		
        	try {
        		Properties prop = GeneralUtils.getProperties("config/config.properties");
    			directory = prop.getProperty("emoticons_directory");
        	} catch (Exception e) {
        		System.err.println("[SentenceBuilder.WiktionaryLookup] " + e);
        		e.printStackTrace();
        	}
    		
			return new EmoticonDictionary(directory + "emoticon_dictionary.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
    }
    
    private AffectFeatures checkEmoticonsForAffectScore(String word) {
    	AffectFeatures affect = null;
    	
    	if (_emoticonDictionary.hasEmoticon(word)) {
    		affect = _DAL.get(_emoticonDictionary.getDefinition(word));
    	}
    	
    	return affect;
    }
    

    private AffectFeatures checkWordLengthening(String word) {
    	AffectFeatures affect = null;
    	
    	if (word.matches(".*(\\w)(\\1)+.*")) {
    		// check for words like sweeeeet
    		word = word.replaceAll("(\\w)(\\1)(\\1)+", "$1$1");
    		if (_DAL.hasWord(word)) {
    			affect = _DAL.get(word);
    			return affect;
    		}
    		// check for words like hiiiiii
    		word = word.replaceAll("(\\w)(\\1)+", "$1");
    		if (_DAL.hasWord(word)) {
    			affect = _DAL.get(word);
    			return affect;
    		}
    	}
    	
    	return affect;
    }
    
    /**
     * Looks up affect score for the given word
     */
    private AffectFeatures getAffectScores(String word, String pos) {
    	
    	// first check emoticons/punctuation
    	if (_useEmoticons && _emoticonDictionary.hasEmoticon(word)) {
        	return checkEmoticonsForAffectScore(word); 
        }
        else if (word.matches("\\p{Punct}+")) 
        	{
        	  return null;
        	}
    	
    	// function word
    	if (isFunctionWord(pos)) 
    		{
    		  return null;
    		}
    	
        AffectFeatures affect = _DAL.get(word);
        HashMap<String,PolarWord> scores = new HashMap<String,PolarWord>();
        
        // don't do anything extra if we are not using any dictionaries 
        if (affect == null && (_useEmoticons || _useWordNet || _useWiktionary)) {
        	boolean inWordnet = false;
        	
        	// proper nouns are automatically objective
            if (_useNoun != null && _useNoun.equals("NN") && getJwnlPos(pos) == POS.NOUN
        			|| _useNoun != null && _useNoun.equals("NNP") && pos.matches("NNP|NNPS")) {
            	return new AffectFeatures(0,0,3,0);
            	
            }
            
            // check for word lengthening (eg. hiiiiii -> hi) if using one of the "sm" dictionaries
            if (_useWiktionary || _useEmoticons) {
            	affect = checkWordLengthening(word);
            	if (affect != null) return affect;
            }
            if (_useWordNet && word.indexOf("_") < 0) {
        		inWordnet = WordNetLookup(scores, word, pos,1);
        	}
            if (_useWiktionary && !inWordnet) //scores.isEmpty())
        		WiktionaryLookup(scores, word,pos,1);
            if (scores.isEmpty()) {
        		return null;
        	}
        	return getDictionariesScore(word,pos,scores).getAffectScores();
        }
        return affect;
    }
    
    /**
     * Lookup word using closest WordNet neighbor
     */
    private boolean WordNetLookup(HashMap<String,PolarWord> scores, String word, String pos, int degree) {
    	if (degree == 3) return false;
    	
        try {
            if (! JWNL.isInitialized()) {
            	System.out.println("Initializing JWNL now!");
                JWNL.initialize(new FileInputStream("config/file_properties.xml"));
            }
            
            
            
            
            POS p = getJwnlPos(pos);
            if (p == null) return false;
            IndexWord w = Dictionary.getInstance().getIndexWord(p, word);
            //if (w.getLemma() != word && word.matches(".*\\{punct}+.*"))
            // check base form if was not found
            if (w == null && word.matches("[A-Za-z]*")) {
            	w = Dictionary.getInstance().getIndexWord(p,Morphology.lemmaStatic(word,pos,true));
            	//w = Dictionary.getInstance().getMorphologicalProcessor().lookupBaseForm(p, word);
            }
            
            if (w == null) return false;
            
            // word in morphological form may exist in the DAL!
            AffectFeatures senses = _DAL.get(w.getLemma().toLowerCase());
            if(senses != null) {
            	scores.put(w.getLemma(),new PolarWord(w.getLemma(),p.toString(),senses, PolarWord.WORDNET,0));
            	return true;
            }
            
            for (Synset synonyms : w.getSenses()) {
                for (Word synonym : synonyms.getWords()) {
                    senses = _DAL.get(synonym.getLemma().toLowerCase());
                    if(senses != null) {
                    	senses.setWeight(computeWeight(pos,synonym.getPOS(),degree));
                    	scores.put(synonym.getLemma(),new PolarWord(synonym.getLemma(),p.toString(),senses,PolarWord.WORDNET,degree));
                    }
                }
                // only look at the first sense as that is the most common one. (WSD!!)
                if (senses == null) {
                	PointerTargetNodeList hypernyms = PointerUtils.getInstance().getDirectHypernyms(synonyms);
                	//System.out.println("after PointerUtils.getInstance().getDirectHypernyms(synonyms)");
                	for (Object hypernym : hypernyms) {
                		if (((PointerTargetNode)hypernym).isLexical()) {
                			WordNetLookup(scores,((PointerTargetNode)hypernym).getWord().getLemma(),pos,degree+1);
                			//System.out.println("after WordNetLookup(scores,((PointerTargetNode)hypernym).getWord().getLemma(),pos,degree+1)");
                		}
                		else {
                			Synset synsets = ((PointerTargetNode)hypernym).getSynset();
                			for (Word hypernymWord : synsets.getWords()) {
                				WordNetLookup(scores,hypernymWord.getLemma(),pos,degree+1);
                			}
                		}
                	}
                }
                break;
            }
            
        } catch(IOException e) {
        	throw new RuntimeException("I/O error while initializing WordNet", e);
        } catch(JWNLException e) {
        	throw new RuntimeException("Error while reading WordNet", e);
        }
        
        return true;
    }

    private void WiktionaryLookup(HashMap<String,PolarWord> scores, String word, String pos, int degree) {
        
    	if (degree == 3 || getJwnlPos(pos) == null || pos.isEmpty()) return;
    	        
    	List<Definition> definitions = _wiktionary.query(word, true, convertToWiktionaryPos(getJwnlPos(pos)));
    	
        for (Definition definition : definitions) {
        	
        	POS defPOS = getWiktionaryPos(definition.getPOS());
        	
        	// don't examine function words, don't examine if we have POS and it doesn't match
        	//if (defPOS != null && defPOS != getJwnlPos(pos)) continue;
        	
        	// first check forms of (e.g. misspelling)
        	for (String term : definition.getFormsOf()) {
    			addScore(scores, term, defPOS, PolarWord.WIKTIONARY, degree);
        	}
            if (!scores.isEmpty()) return;
        	
            // now check related terms
    		for (String term : definition.getRelatedTerms()) {
    			addScore(scores, term, defPOS, PolarWord.WIKTIONARY, degree);
        	}
    		if (!scores.isEmpty()) return;
    		
    		// look up the "form of" version (e.g. hottie for hotties)
    		for (String term : definition.getFormsOf()) {
    			WiktionaryLookup(scores, term, pos, degree+1);    			        			
        	}
        }
    }

    private void addScore(HashMap<String,PolarWord> scores, String term, POS POS, int dictionary, int degree) {
		AffectFeatures senses = _DAL.get(term.toLowerCase());
		
		if (term.indexOf("|") > 0) {
			for (String t : term.split("\\|")) {
    			senses = _DAL.get(t.toLowerCase());
    			if (senses != null) break;
			}
		}
		        			
        if(senses != null) {
        	//senses.setWeight(computeWeight(pos,defPOS,degree));
        	scores.put(term,new PolarWord(term,POS == null ? null : POS.toString(),senses,dictionary,degree));
        }
    }
    
    public static Wiktionary loadWiktionary() {
   
    	String directory = "";
    	try {
    		Properties prop = GeneralUtils.getProperties("config/config.properties");
			directory = prop.getProperty("wiktionary_directory");
    		
    	} catch (Exception e) {
    		System.err.println("[SentenceBuilder.WiktionaryLookup] " + e);
    		e.printStackTrace();
    	}
    	
    	if (!new File(directory).exists()) {
    		
    		System.err.println("[SentenceBuilder.WiktionaryLookup] wiktionary is not installed");
    		return null;
    	}
    	
		return new Wiktionary(directory);
    }
    
	private double computeWeight(String pos, POS defPOS, double degree) {
	
		double weight = 1;
		
		// lower score if different POS or null (initialism etc...)
		if (pos == null) weight -= .1;
		if (!defPOS.equals(getJwnlPos(pos))) weight -= .3;
		weight -= degree * .1;
		
		return weight;
	}

    private PolarWord getDictionariesScore(String word, String tag, HashMap<String,PolarWord> scores) {
    	// average
    	double ee = 0, aa = 0, ii = 0, norm = 0, weight = 0;
    	int s = scores.size();
    	for (PolarWord w : scores.values()) {
    		AffectFeatures score = w.getAffectScores();
    		aa += score.getActivation();
    		ii += score.getImagery();
    		norm += score.getNorm();
    		ee += score.getPleasantness();
    		weight += score.getWeight();
    	}

    	PolarWord average = new PolarWord(word,tag,new AffectFeatures(ee/s,aa/s,ii/s,norm/s,weight/s));
    	return average;
    }
    
    private static boolean isFunctionWord(String pos) {
    	
    	// articles
    	if (pos.matches("DT|TO|EX|PDT")) return true;
    	//pronouns inflected in English, as he  him, she  her, etc.
    	//if (pos.matches("PRP|PRP\\$|WP|WP\\$")) return true;
    	//prepositions and conjunctions 
    	if (pos.matches("CC|IN")) return true;
    	//numbers
    	if (pos.matches("CD")) return true;
    	
    	return false;
    	//auxiliary verbsforming part of the conjugation (pattern of the tenses of main verbs), always inflected
    	//interjections sometimes called \"filled pauses\", uninflected    	
    }
    
    public static POS getJwnlPos(String pos) {
    	if (pos == null) return null;
        if(pos.equals("JJ")) return POS.ADJECTIVE;
        if(pos.matches("NN[A-Z]?")) return POS.NOUN;
        if(pos.equals("RB")) return POS.ADVERB;
        if(pos.matches("VB[A-Z]?")) return POS.VERB;
        
        return null;
	}
    
    public static POS getWiktionaryPos(String pos) {
    	if (pos == null) return null;
        if(pos.equals("Adjective")) return POS.ADJECTIVE;
        if(pos.equals("Noun") || pos.equals("Proper noun")) return POS.NOUN;
        if(pos.equals("Adverb")) return POS.ADVERB;
        if(pos.equals("Verb")) return POS.VERB;
        
        return null; 
	}
    
    public static String convertToWiktionaryPos(POS pos) {
    	if (pos == null) return null;
    	else if(pos == POS.ADJECTIVE) return "Adjective";
    	else if(pos == POS.NOUN) return "Noun";
    	else if(pos == POS.ADVERB) return "Adverb";
    	else if(pos == POS.VERB) return "Verb";
        
        return null; 
	}
}
