package edu.columbia.opinion.process;

import java.util.ArrayList;
//import java.util.List;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import edu.columbia.opinion.classifications.Classification;

public class PolarityDataProcessor extends TestDataProcessor {

    public PolarityDataProcessor(Classification classification, boolean socialMedia, boolean sentiWordNet,
    		int nGrams, boolean tuneToAnnotations) {
		super(classification,socialMedia,sentiWordNet, nGrams,tuneToAnnotations);
	}
	
	protected ArrayList<Attribute> getAttributes() {
		return getAttributesPolarity();
	}
 	
	protected Instance sentenceToInstance(Sentence sentence, Instances dataset) {
        return sentenceToInstancePolarity(sentence, dataset);
    }

}
