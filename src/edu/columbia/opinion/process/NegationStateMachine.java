package edu.columbia.opinion.process;

/**
 * State machine for tracking whether words in a phrase are actually "positive"
 * or "negative" in their context
 * 
 * @author Jacob modified by Or
 */
public class NegationStateMachine {

    private static enum State {
        INVERT, RETAIN
    }

    private State _state;

    public NegationStateMachine()  {
        _state = State.RETAIN;
    }
    
    public int getSign() {
        return _state == State.RETAIN ? 1 : -1;
    }

    public void next(String word, String pos) {
        if(_state == State.INVERT) {
            if(word.matches("but|only|not|never|cannot") || pos.startsWith("JJR")) { // TODO should this be JJ?
                _state = State.RETAIN;
            }
        } else { // state == States.RETAIN
            if(word.matches("not|no|never|cannot|.*?n't$")) {
                _state = State.INVERT;
            }
        }
    }

}
