package edu.columbia.opinion.process;

import java.util.*;
import processing.GeneralUtils;

/**
 * Provides a singleton dictionary, storing affect scores from the DAL
 * 
 * @author Jacob, modified by Or
 */
public class DAL {
    
	// see http://www.hdcus.com/manuals/wdalman.pdf
    private static final double MEAN_PLEASANTNESS = 1.85; //1.84;
    private static final double MEAN_ACTIVATION = 1.67; //1.85;
    private static final double MEAN_IMAGERY = 1.54; //1.94;
    private static final double SD_PLEASANTNESS = .36; //.44;
    private static final double SD_ACTIVATION = .36; //.39;
    private static final double SD_IMAGERY = .63;
    
    // singleton instance
    private static DAL _dal;

    private Map<String, AffectFeatures> _dictionary;
    private String _inputFile = "data/DAL.txt";
    
    private DAL() {
        _dictionary = new HashMap<String,AffectFeatures>();
        
    	try {
    		Properties prop = GeneralUtils.getProperties("config/config.properties");
			_inputFile = prop.getProperty("DAL");
    		
    	} catch (Exception e) {
    		System.err.println("[SentenceBuilder.WiktionaryLookup] " + e);
    		e.printStackTrace();
    	}
        
        loadDAL();
    }

    // loads dictionary into memory
    private void loadDAL() {
        String filename = _inputFile;

        for (String line : Utils.readLines(filename)) {
        	String[] parts = line.split("\\s+");
        	String word = parts[0];
        	
        	double pleasantness = (Double.parseDouble(parts[1]) - MEAN_PLEASANTNESS) / SD_PLEASANTNESS;
        	double activation = (Double.parseDouble(parts[2]) - MEAN_ACTIVATION) / SD_ACTIVATION;
        	double imagery = (Double.parseDouble(parts[3])- MEAN_IMAGERY) / SD_IMAGERY;
        	double norm = Math.sqrt(pleasantness*pleasantness + activation*activation) / (imagery + 0.001);
        	
        	_dictionary.put(word, new AffectFeatures(pleasantness, activation, imagery, norm));
        }
    }

    public static DAL getInstance() {
        if(DAL._dal == null) {
            DAL._dal = new DAL();
        }
        return DAL._dal;
    }
    
    public boolean hasWord(String word) {
    	if (this.get(word) == null) return false;
    	return true;
    }

    public AffectFeatures get(String word) {
        return _dictionary.get(word.toLowerCase());
    }
}
