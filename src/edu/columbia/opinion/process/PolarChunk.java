package edu.columbia.opinion.process;

import java.util.*;

/**
 * Represents a sentence chunk tagged with position and polarity
 * 
 * @author Jacob, modified by Or
 */
public class PolarChunk {

	public static final int NUM_POSITIONS = 3;
	public static final int NUM_TAGS = 5;

	public static final int UNASSIGNED = -1;
	
    public static final int NEGATIVE = 0;
    public static final int NEUTRAL = 1;
    public static final int POSITIVE = 2;
    
    public static final int SUBJECTIVE = 0;
    public static final int OBJECTIVE = 1;

    public static final int BEFORE = 0;
    public static final int TARGET = 1;
    public static final int AFTER = 2;

    private static final int NP = 0;
    private static final int VP = 1;
    private static final int ADJP = 2;
    private static final int PP = 3;
    private static final int OTHER = 4;
    
    private List<PolarWord> _words;
    private int _tag;
    private int _position;
    private int _polarity;
    private double _meanPleasantness;
    private double _meanImagery;
    private boolean _opinion = true;

    public PolarChunk(String tagStr, boolean opinion) {
        _words = new ArrayList<PolarWord>();
        _tag = tagFromString(tagStr);
        _polarity = UNASSIGNED;
        _opinion = opinion;
    }
  
    private int tagFromString(String tagStr) {
        if (tagStr.equals("NP")) return NP;
        if (tagStr.equals("VP")) return VP;
        if (tagStr.equals("AJDP")) return ADJP;
        if (tagStr.equals("PP")) return PP;
        
        return OTHER;
	}
    
    private String tagToString(int tag) {
        if (tag == NP) return "NP";
        if (tag == VP) return "VP";
        if (tag == ADJP) return "ADJP";
        if (tag == PP) return "PP";
        
        return "";
	}
    
    private String positionToString(int pos) {
    	
    	if (pos == BEFORE) return "";
    	if (pos == AFTER) return "";
    	if (pos == TARGET) return "_T";
    	return "";
    }
    
    private String polarityToString(int pol) {
    	if (_opinion) {
    		if (pol == SUBJECTIVE) return "s";
        	if (pol == OBJECTIVE) return "o";
    	}
    	else {
    		if (pol == POSITIVE) return "+";
    		if (pol == NEGATIVE) return "-";
    		if (pol == NEUTRAL) return "o";
    	}
    	return "";
    }

	public void setPosition(int pos) {
		if (pos >= NUM_POSITIONS) {
			System.err.println("Invalid Position " + pos);
		}
        this._position = pos;
    }

    public void setPolarity(int pol) {
        this._polarity = pol;
    }

    public int getPosition() {
        return _position;
    }

    public int getPolarity() {
        return _polarity;
    }

    public void add(PolarWord w) {
        _words.add(w);
    }

    public PolarWord get(int i) {
        return _words.get(i);
    }
    
    public String getPlainString() {
    	String str = "";
    	for (PolarWord word : _words) {
    		str += word.getWord() + " ";
    	}
    	return str.trim();
    }

    public int length() {
        return _words.size();
    }

    public String toString() {
        return _words.toString() + "/" + tagToString(_tag) + positionToString(_position) + "[" + polarityToString(getPolarity()) + "] ";
    }

    public List<PolarWord> getTaggedWords() {
        return _words;
    }

    public int getTag() {
        return _tag;
    }

    public void setMeanPleasantness(double meanPleasantness) {
        this._meanPleasantness = meanPleasantness;
    }

    public double getMeanPleasantness() {
        return _meanPleasantness;
    }

	public double getMeanImagery() {
		return _meanImagery;
	}

	public void setMeanImagery(double meanImagery) {
		_meanImagery = meanImagery;
	}

    
}
