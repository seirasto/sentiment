package edu.columbia.opinion.process;

import java.util.ArrayList;

import edu.columbia.opinion.classifications.Classification;
//import social_media.Features;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

//import java.util.Hashtable;
//import java.util.regex.*;

public class SubjectivityDataProcessor extends TestDataProcessor {

	/*private static final String MIN_PLEASANTNESS_ATT = "min_ee";
	private static final String MAX_PLEASANTNESS_ATT = "max_ee";
	private static final String [] SM_TEXT_FEATURES = new String [] {";\\)",":\\(",";-\\)","\\^_\\^","!\\?","-",",","\\.",":","@","#","\\(","\\)","\"","&","~","`","'","\\+","\\$","idk","imo","u","lol","k","\\*"};
*/
    public SubjectivityDataProcessor(Classification classification, boolean socialMedia, boolean sentiWordNet, int nGrams, boolean tuneToAnnotations) {
		super(classification,socialMedia, sentiWordNet, nGrams,tuneToAnnotations);
	}
    	
	protected ArrayList<Attribute> getAttributes() {
		return getAttributesSubjectivity();
}
	
protected Instance sentenceToInstance(Sentence sentence, Instances dataset) {
    return sentenceToInstanceSubjectivity(sentence, dataset);
}
}
